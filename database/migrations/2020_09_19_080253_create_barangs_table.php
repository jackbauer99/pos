<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateBarangsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master.barang', function (Blueprint $table) {
            $table->id();
            $table->char('uuid',36)->unique();
            $table->string('kode_barang');
            $table->string('nama_barang');
            $table->char('uuid_kategori',36);
            $table->string('manufaktur_barang');
            $table->integer('jumlah_stok_barang');
            $table->integer('harga_per_satuan_barang');
            $table->integer('is_habis')->default(0);
            $table->integer('is_aktif')->default(0);
            $table->softDeletes();
            $table->timestamps();

            $table->foreign('uuid_kategori')->references('uuid')->on('master.kategori_barang')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master.barang');
    }
}
