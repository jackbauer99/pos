<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateStatusKepegawaiansTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kepegawaian.status_kepegawaian', function (Blueprint $table) {
            $table->id();
            $table->char('uuid',36)->unique();
            $table->string('nama_status_kepegawaian');
            $table->string('lama_kerja_status_kepegawaian');
            $table->integer('gaji_pokok_status_kepegawaian');
            $table->integer('is_aktif')->default(0);
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kepegawaian.status_kepegawaian');
    }
}
