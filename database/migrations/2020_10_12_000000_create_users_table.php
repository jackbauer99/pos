<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('auth.user', function (Blueprint $table) {
            $table->id();
            $table->string('uuid',36)->unique();
            $table->char('uuid_pegawai',36);
            $table->char('uuid_role',36);
            $table->string('username');
            $table->string('password');
            $table->integer('is_login')->default(0);
            // $table->timestamp('email_verified_at')->nullable();
            // $table->string('password');
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('uuid_pegawai')->references('uuid')->on('kepegawaian.pegawai')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('uuid_role')->references('uuid')->on('master.role')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('auth.user');
    }
}
