<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreatePegawaisTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('kepegawaian.pegawai', function (Blueprint $table) {
            $table->id();
            $table->char('uuid',36)->unique();
            $table->string('nip_pegawai')->unique();
            $table->string('nama_pegawai');
            $table->char('uuid_jabatan',36);
            $table->string('tempat_lahir_pegawai')->nullable();
            $table->date('tanggal_lahir_pegawai')->nullable();
            $table->string('agama_pegawai')->nullable();
            $table->char('jenis_kelamin_pegawai',1)->nullable();
            $table->string('status_nikah_pegawai')->nullable();
            $table->char('telepon1_pegawai',30)->nullable();
            $table->char('telepon2_pegawai',30)->nullable();
            $table->string('email_pegawai')->nullable();
            $table->string('alamat_pegawai')->nullable();
            $table->char('uuid_status',36);
            //$table->integer('lama_tahun_bekerja_pegawai');
            $table->string('foto_pegawai')->nullable();
            $table->integer('is_aktif')->default(0);
            $table->integer('is_akses')->default(0);
            $table->timestamps();
            $table->softDeletes();

            $table->foreign('uuid_jabatan')->references('uuid')->on('kepegawaian.jabatan')->onDelete('cascade')->onUpdate('cascade');
            $table->foreign('uuid_status')->references('uuid')->on('kepegawaian.status_kepegawaian')->onDelete('cascade')->onUpdate('cascade');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('kepegawaian.pegawai');
    }
}
