<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateLogsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('master.logs', function (Blueprint $table) {
            $table->id();
            $table->char('uuid',36)->unique();
            $table->char('uuid_user',36);
            $table->string('activity')->nullable();
            $table->string('user_ipaddress');
            $table->string('user_device')->nullable();
            $table->string('user_browser')->nullable();
            $table->string('user_location')->nullable();
            $table->date('last_login')->nullable();
            $table->text('action_data')->nullable();
            $table->string('execution_time')->nullable();
            $table->string('status')->nullable();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('master.logs');
    }
}
