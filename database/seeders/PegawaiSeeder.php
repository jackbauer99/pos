<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Pegawai;
use App\Models\Jabatan;
use App\Models\StatusKepegawaian;
use App\Models\Role;
use App\Helper\Date;
use Faker\Factory as Faker;

class PegawaiSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        $faker = Faker::create('id_ID');
        $birthplace = array(
          0 => 'Surabaya',
          1 => 'Lamongan',
          2 => 'Gresik',
          3 => 'Nganjuk',
          4 => 'Kediri',
          5 => 'Pamekasan',
          6 => 'Blitar',
          7 => 'Pasuruan',
          8 => 'Malang'
        );
        $num = 9000;
        $jk = ['L','P'];
        Pegawai::create([
          'nip_pegawai' => 'admin',
          'nama_pegawai' => 'Super Administrator',
          'uuid_jabatan' => Jabatan::find(1)->uuid,
          'tempat_lahir_pegawai' => $birthplace[rand(0,8)],
          'tanggal_lahir_pegawai' => Date::getRandomDateBirth(),
          //'agama_pegawai' => Pegawai::getAgama(rand(0,5)),
          'jenis_kelamin_pegawai' => $jk[mt_rand(0,1)],
          //'status_nikah_pegawai' => Pegawai::getSatusNikah(rand(0,3)),
          'telepon1_pegawai' => $faker->unique()->phoneNumber,
          'telepon2_pegawai' => $faker->unique()->phoneNumber,
          'email_pegawai' => $faker->unique()->freeEmail,
          'alamat_pegawai' => $faker->address,
          'uuid_status' => $status = StatusKepegawaian::where('id',1)->firstOrFail()->uuid,
          //'lama_tahun_bekerja_pegawai' => $status->lama_kerja_status_kepegawaian
        ]);

        // for($i=1;$i<=200;$i++) {
        //   $rand = rand(1,4);
        //   $status = StatusKepegawaian::where('id',$rand)->firstOrFail()->uuid;
        //   Pegawai::create([
        //     'nip_pegawai' => '57754464466'.$num++,
        //     'nama_pegawai' => $faker->name,
        //     'uuid_jabatan' => Jabatan::find(rand(1,4))->uuid,
        //     'tempat_lahir_pegawai' => $birthplace[rand(0,8)],
        //     'tanggal_lahir_pegawai' => Date::getRandomDateBirth(),
        //     'agama_pegawai' => rand(1,6),
        //     'jenis_kelamin_pegawai' => $jk[mt_rand(0,1)],
        //     'status_nikah_pegawai' => rand(1,4),
        //     'telepon1_pegawai' => $faker->unique()->phoneNumber,
        //     'telepon2_pegawai' => $faker->unique()->phoneNumber,
        //     'email_pegawai' => $faker->unique()->freeEmail,
        //     'alamat_pegawai' => $faker->address,
        //     'uuid_status' => $status,
        //     //'lama_tahun_bekerja_pegawai' => $status->lama_kerja_status_kepegawaian
        //   ]);
        // }
    }
}
