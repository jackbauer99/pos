<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\User;
use App\Models\Pegawai;
use App\Models\Role;
use Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        User::create([
          'uuid_pegawai' => Pegawai::find(1)->uuid,
          'username' => Pegawai::find(1)->nip_pegawai,
          'password' => Hash::make('123456789'),
          'uuid_role' => Role::find(1)->uuid
        ]);

        Pegawai::find(1)->update([
          'is_akses' => 1
        ]);
    }
}
