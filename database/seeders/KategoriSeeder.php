<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\KategoriBarang;
use Ramsey\Uuid\Uuid;

class KategoriSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        KategoriBarang::create([
          'uuid' => Uuid::uuid3(Uuid::NAMESPACE_DNS,'Detergen')->toString(),
          'nama_kategori' => 'Detergen'
        ]);
        KategoriBarang::create([
          'uuid' => Uuid::uuid3(Uuid::NAMESPACE_DNS,'Kue')->toString(),
          'nama_kategori' => 'Kue'
        ]);
        KategoriBarang::create([
          'uuid' => Uuid::uuid3(Uuid::NAMESPACE_DNS,'Makanan Ringan')->toString(),
          'nama_kategori' => 'Makanan Ringan'
        ]);
    }
}
