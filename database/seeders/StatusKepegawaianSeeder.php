<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\StatusKepegawaian;

class StatusKepegawaianSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        StatusKepegawaian::create([
          'nama_status_kepegawaian' => 'Tetap',
          'lama_kerja_status_kepegawaian' => 5,
          'gaji_pokok_status_kepegawaian' => 5000000
        ]);

        StatusKepegawaian::create([
          'nama_status_kepegawaian' => 'PNS',
          'lama_kerja_status_kepegawaian' => 10,
          'gaji_pokok_status_kepegawaian' => 15000000
        ]);

        StatusKepegawaian::create([
          'nama_status_kepegawaian' => 'Kontrak',
          'lama_kerja_status_kepegawaian' => 3,
          'gaji_pokok_status_kepegawaian' => 3500000
        ]);

        StatusKepegawaian::create([
          'nama_status_kepegawaian' => 'Magang',
          'lama_kerja_status_kepegawaian' => 3,
          'gaji_pokok_status_kepegawaian' => 0
        ]);
    }
}
