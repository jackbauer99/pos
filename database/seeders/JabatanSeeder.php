<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Jabatan;

class JabatanSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Jabatan::create([
          'kode_jabatan' => '123456789101234',
          'nama_jabatan' => 'Super Visor'
        ]);

        Jabatan::create([
          'kode_jabatan' => '123456789101235',
          'nama_jabatan' => 'Front Desk'
        ]);

        Jabatan::create([
          'kode_jabatan' => '123456789101236',
          'nama_jabatan' => 'Tukang IT'
        ]);

        Jabatan::create([
          'kode_jabatan' => '123456789101237',
          'nama_jabatan' => 'Tukang Kebun'
        ]);
    }
}
