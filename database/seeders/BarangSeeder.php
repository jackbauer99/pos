<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Barang;
use App\Models\KategoriBarang;

class BarangSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Barang::create([
          'kode_barang' => '456633225',
          'nama_barang' => 'Sari Roti Pandan Srikaya',
          'uuid_kategori' => KategoriBarang::find(1)->uuid,
          'manufaktur_barang' => 'Sari Roti',
          'jumlah_stok_barang' => 100,
          'harga_per_satuan_barang' => 5000
        ]);
    }
}
