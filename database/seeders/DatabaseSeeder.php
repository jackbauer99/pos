<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // User::factory(10)->create();
        $this->call([
          // KategoriSeeder::class,
          // BarangSeeder::class,
          JabatanSeeder::class,
          StatusKepegawaianSeeder::class,
          RoleSeeder::class,
          PegawaiSeeder::class,
          UserSeeder::class
        ]);
    }
}
