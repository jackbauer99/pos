<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Role;
use App\Models\Jabatan;

class RoleSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        Role::create([
          'nama_role' => 'Super Admin',
          // 'uuid_jabatan' => Jabatan::find(rand(1,4))->uuid
        ]);

        Role::create([
          'nama_role' => 'Level 1'
        ]);

        Role::create([
          'nama_role' => 'Level 2'
        ]);
    }
}
