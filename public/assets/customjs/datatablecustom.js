$(document).ajaxStart(function() {
  $.blockUI({
    css: {
      border: 'none',
      padding: '15px',
      backgroundColor: '#000',
      '-webkit-border-radius': '10px',
      '-moz-border-radius': '10px',
      opacity: .5,
      color: '#fff',
    },
    message: '<i class="fas fa-spinner fa-pulse"></i>  Mohon Tunggu Sebentar'
  })
});

let CustomDataTables = (_table, _url, _data = null, _column, _dataType = 'json') => {
  //console.log(_column)
  let column = [];
  let name;
  $.each(_column,function(i, val) {
    column.push({
      data: val
    });
  });

  console.log(column)

  let _datatable = _table.DataTable({
    processing: true,
    serverside: true,
    ajax : {
      url : _url,
      type : 'POST',
      dataType : _dataType,
    },
    columns: column,
  });
}

$(document).ajaxStop(function() {
  $.unblockUI();
});

let SendData = (_form, _url, _form_id = null, _select2 = null, datatable) => {
  // _form_id.on('submit', function() {
  //   event.preventDefault();
  Swal.fire({
    title: 'Apakah Anda Yakin Menyimpan Data Ini ?',
    // showDenyButton: true,
    showCancelButton: true,
    confirmButtonText: `Simpan`,
    confirmButtonColor: '#d33',
    // denyButtonText: `Kembali`,
    icon: 'question'
  }).then((result) =>{
    if(result.isConfirmed) {
      $.ajax({
        url: _url,
        method: "POST",
        data: _form,
        contentType: false,
        cache: false,
        processData: false,
        dataType: "json",
        success:function(data) {
          var count
          if(data.response == 'success') {
            Swal.fire(
              data.header,
              data.messages,
              'success'
            )
            datatable.DataTable().ajax.reload()
            if(_form_id != null) {
                _form_id[0].reset()
                // _form_id.trigger('reset');
            }
            if(_select2 != null) {
              _select2.val('').trigger('change');
            }
          } else if(data.response == 'failed') {
            var messages = data.messages.join('<br/>')
            //console.log(messages)
            // Swal.fire(
            //   data.header,
            //   messages,
            //   'error'
            // )
            toastr.error(messages);
          }
        }
      });
    } else if(result.isDenied) {
      Swal.fire('Changes are not saved', '', 'info')
    }
  })
}

let DeleteData = (_url,datatable) => {
  Swal.fire({
    title: 'Apakah Anda Yakin Menghapus Data Ini ?',
    // showDenyButton: true,
    showCancelButton: true,
    confirmButtonText: `Hapus`,
    confirmButtonColor: '#d33',
    // denyButtonText: `Kembali`,
    icon: 'question'
  }).then((result) => {
  /* Read more about isConfirmed, isDenied below */
    if (result.isConfirmed) {
      $.ajax({
        url: _url,
        method: "DELETE",
        dataType: "json",
        success:function(data) {
          if(data.response == 'success') {
              Swal.fire(data.header, data.messages, 'success')
              datatable.DataTable().ajax.reload()
          } else if(data.response == 'failed') {
            Swal.fire(data.header, data.messages, 'error')
          }
        }
      })

    } else if (result.isDenied) {
      Swal.fire('Changes are not saved', '', 'info')
    }
  });
}
