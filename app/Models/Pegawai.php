<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Pegawai extends Main
{
    protected $table = 'kepegawaian.pegawai';

    protected $fillable = [
      'nip_pegawai',
      'nama_pegawai',
      'uuid_jabatan',
      'tempat_lahir_pegawai',
      'tanggal_lahir_pegawai',
      'agama_pegawai',
      'jenis_kelamin_pegawai',
      'status_nikah_pegawai',
      'telepon1_pegawai',
      'telepon2_pegawai',
      'email_pegawai',
      'alamat_pegawai',
      'uuid_status',
    //  'lama_tahun_bekerja_pegawai',
      'foto_pegawai',
      'is_aktif',
      'is_akses'
    ];

    public static function getAgama($idAgama = null) {
      $agama = array(
        1 => 'Islam',
        2 => 'Kristen Protestan',
        3 => 'Kristen Katholik',
        4 => 'Hindu',
        5 => 'Budha',
        6 => 'Konghucu'
      );

      if(!empty($idAgama)) {
        return $agama[$idAgama];
      } else {
          return $agama;
      }
    }

    public static function getSatusNikah($idNikah = null) {
      $nikah = array(
        1 => 'Belum Menikah',
        2 => 'Menikah',
        3 => 'Cerai Hidup',
        4 => 'Cerai Mati'
      );

      if(!empty($idNikah)) {
        return $nikah[$idNikah];
      } else {
        return $nikah;
      }
    }

    public static function getJenisKelamin($idJK = null) {
      $jk = array(
        'L' => 'Laki-laki',
        'P' => 'Perempuan'
      );

      if(!empty($idJK)) {
        return $jk[$idJK];
      } else {
        return $jk;
      }
    }

    public static function getStatusPegawai($idSP = null) {
      // $SP = array(
      //   'Pegawai Tetap' => array(
      //     'lama-kerja' => null,
      //     'gaji-pokok' => 2500000
      //   ),
      //   'Pegawai Kontrak' => array(
      //     'lama-kerja' => 1,
      //     'gaji-pokok' => 18
      //   )
      // )
    }

    public function StatusKepegawaian() {
      return $this->belongsTo(StatusKepegawaian::class,'uuid_status','uuid');
    }

    public function Jabatan() {
      return $this->belongsTo(Jabatan::class,'uuid_jabatan','uuid');
    }

    public function User() {
      return $this->belongsTo(User::class,'uuid','uuid_pegawai');
    }
}
