<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Barang extends Main
{
    protected $table = 'master.barang';

    protected $fillable = [
      'kode_barang',
      'nama_barang',
      'uuid_kategori',
      'manufaktur_barang',
      'jumlah_stok_barang',
      'harga_per_satuan_barang'
    ];

    public function KategoriBarang() {
      return $this->belongsTo(KategoriBarang::class,'uuid_kategori','uuid');
    }
}
