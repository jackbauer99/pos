<?php

namespace App\Models;

class KategoriBarang extends Main
{
    protected $table = 'master.kategori_barang';

    protected $fillable = [
      'nama_kategori',
    ];
}
