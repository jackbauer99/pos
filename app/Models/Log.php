<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Log extends Main
{
    protected $table = 'master.logs';

    protected $fillable = [
      'uuid_user',
      'activity',
      'user_ipaddress',
      'user_device',
      'user_browser',
      'user_location',
      'last_login',
      'action_data',
      'execution_time',
      'status'
    ];

    public function User() {
      return $this->belongsTo(User::class,'uuid_user','uuid');
    }

    public function pegawai() {
      return $this->belongsTo(Pegawai::class,'uuid_user','uuid');
    }
}
