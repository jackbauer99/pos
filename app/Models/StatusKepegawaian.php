<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class StatusKepegawaian extends Main
{
    protected $table = 'kepegawaian.status_kepegawaian';

    protected $fillable = [
      'nama_status_kepegawaian',
      'lama_kerja_status_kepegawaian',
      'gaji_pokok_status_kepegawaian',
      'is_aktif'
    ];
}
