<?php

namespace App\Models;

use Illuminate\Contracts\Auth\MustVerifyEmail;
use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Notifications\Notifiable;
use App\Traits\Uuid;

class User extends Authenticatable
{
    use HasFactory, Notifiable, Uuid;

    protected $table = 'auth.user';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'username','password','uuid_role','uuid_pegawai','is_login'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function Role(){
      return $this->belongsTo(Role::class,'uuid_role','uuid');
    }

    public function Pegawai(){
      return $this->belongsTo(Pegawai::class,'uuid_pegawai','uuid');
    }
}
