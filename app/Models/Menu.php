<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Menu extends Model
{
    public static function SideBarMenu() {
      $menu['Menu'] = array(
        // 'Master' => array(
        //   'icon' => 'fa fa-database',
        //   'childmenu' => array(
        //     'Barang' => 'barang.index',
        //     'Kategori' => 'kategori.index'
        //   ),
        // ),
        'Dashboard' => array(
          'icon' => 'fa fa-tachometer-alt',
          'url' => 'dashboard.index'
        ),
        'Kepegawaian' => array(
          'icon' => 'fa fa-user',
          'childmenu' => array(
            'Status Kepegawain' => 'skp.index',
            'Jabatan' => 'jabatan.index',
            'Pegawai' => 'pegawai.index',
          ),
          'access' => 1
        ),
        // 'Maps' => array(
        //   'url' => 'leaflet.index',
        //   'icon' => 'fa fa-gear'
        // ),
      );
      return $menu;
    }
}
