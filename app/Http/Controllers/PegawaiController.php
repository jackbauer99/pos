<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Pegawai;
use App\Models\StatusKepegawaian;
use App\Models\Jabatan;
use App\Models\Validation;
use App\Models\User;
use App\Models\Role;
use DB;
use DataTables;
use Validator;
use Storage;
use Hash;
use Auth;

class PegawaiController extends Controller
{

    public function datatables() {
      $query = Pegawai::with([
        'StatusKepegawaian',
        'Jabatan'
        ])->select('pegawai.*')->where('uuid','!=',Auth::user()->pegawai->uuid)->where('nip_pegawai','!=','admin');
        return Datatables::of($query)
               ->addIndexColumn()
               ->addColumn('action', function($data) {
                 $button = '<button type="button" name="view" id="'.route('pegawai.show',$data->uuid).'" class="view btn btn-xs btn-primary" data-toggle="tooltip" title="Lihat Data"><i class = "fa fa-eye"></i></button>';
                 $button .= '<button type="button" name="delete" id="'.route('pegawai.destroy',$data->uuid).'" class="delete btn btn-danger btn-xs" data-toggle="tooltip" title="Hapus"><i class = "fa fa-trash"></i></button> ';
                 $button .= '<button type="button" name="update" id="'.route('pegawai.edit',$data->uuid).'" class="update btn btn-warning btn-xs" data-toggle="tooltip" title="Edit"><i class = "fa fa-edit"></i></button>';
                 if($data->is_akses == 0) {
                    $button .= '<button type="button" name="update" id="'.route('auth.requestLoginUser',$data->uuid).'" class="akses btn btn-success btn-xs" data-toggle="tooltip" title="Beri Akses"><i class = "fa fa-user"></i></button>';
                 }
                 return $button;
               })
               ->rawColumns(['action'])
               ->make(true);
    }

    public function requestAct($act) {

      if($act == 'add') {
        return response()->json([
          'response' => 'success'
        ]);
      } else if($act == 'upd') {
        return response()->json([
          'response' => 'success'
        ]);
      } else if($act == 'read') {
        return response()->json([
          'response' => 'success'
        ]);
      } else {
        return response()->json([
          'response' => 'failed'
        ]);
      }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('AdminLTE.pages.pegawai.list');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
        $data['title'] = 'Tambah Pegawai';
        $data['action'] = 'simpan';
        $data['agama'] = Pegawai::getAgama();
        $data['statusnikah'] = Pegawai::getSatusNikah();
        $data['jk'] = Pegawai::getJenisKelamin();
        $data['jabatan'] = DB::table('kepegawaian.jabatan')
                           ->select('uuid','nama_jabatan')
                           ->where('deleted_at',null)
                           ->get();
        $data['skp'] = DB::table('kepegawaian.status_kepegawaian')
                           ->select('uuid','nama_status_kepegawaian')
                           ->where('deleted_at',null)
                           ->get();
        $data['role'] = DB::table('master.role')
                            ->select('uuid','nama_role')
                            ->where('deleted_at',null)
                            ->get();
        $data['allow-button'] = true;
        return view('AdminLTE.pages.pegawai.form',compact('data'));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = array(
          'nip_pegawai' => 'required|min:15',
          'nama_pegawai' => 'required',
          'uuid_jabatan' => 'required',
          'tempat_lahir' => 'required',
          'tanggal_lahir' => 'required',
          'agama' => 'required',
          'jk' => 'required',
          'snikah' => 'required',
          'telepon1' => 'required|numeric',
          'telepon2' => 'required|numeric',
          'email' => 'required',
          'alamat' => 'required',
          'statuskep' => 'required',
          'foto' => 'required|image'
        );
        $errors = Validator::make(
          $request->all(),
          $rules,
          Validation::ValidationMessage()
        );

        if($errors->fails()) {
          return response()->json([
            'response' => 'failed',
            'header' => 'Gagal',
            'messages' => $errors->errors()->all(),
          ]);
        }

        DB::beginTransaction();

        $image_path = '/assets/photos/pegawai/';

        $request->request->add([
          'nip_pegawai' => $request->nip_pegawai,
          'nama_pegawai' => $request->nama_pegawai,
          'uuid_jabatan' => $request->uuid_jabatan,
          'tempat_lahir_pegawai' => $request->tempat_lahir,
          'tanggal_lahir_pegawai' => $request->tanggal_lahir,
          'agama_pegawai' => $request->agama,
          'jenis_kelamin_pegawai' => $request->jk,
          'status_nikah_pegawai' => $request->snikah,
          'telepon1_pegawai' => $request->telepon1,
          'telepon2_pegawai' => $request->telepon2,
          'email_pegawai' => $request->email,
          'alamat_pegawai' => $request->alamat,
          'uuid_status' => $request->statuskep,
          //'lama_tahun_bekerja_pegawai' => StatusKepegawaian::where('uuid',$request->statuskep)->firstOrFail()->lama_kerja_status_kepegawaian,
          'foto_pegawai' => '/'.$request->file('foto')->storeAs($image_path,$request->nip_pegawai.'.'.$request->foto->extension())
        ]);

        // $this->beginQueryLog();
        $insert = Pegawai::create($request->all());

        if($insert) {
          $pegawai = Pegawai::where('nip_pegawai',$request->nip_pegawai)->firstOrFail();
          if(!empty($request->role)) {
            User::create([
              'uuid_pegawai' => $pegawai->uuid,
              'username' => $pegawai->nip_pegawai,
              'password' => Hash::make($pegawai->tanggal_lahir_pegawai),
              'uuid_role' => $request->role
            ]);
            $pegawai->update([
              'is_akses' => 1
            ]);
          }
          // dd($this->endQueryLog());
          DB::commit();
          return response()->json([
            'response' => 'success',
            'header' => 'Sukses',
            'messages' => 'Data Sukses Tersimpan',
            'post_data' => $request->all()
          ]);
        } else if(!$insert) {
          DB::rollback();
          return response()->json([
            'response' => 'failed',
            'header' => 'Gagal',
            'messages' => 'Data Gagal Tersimpan',
          ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($uuid)
    {
        //
        if(Auth::user()->pegawai->uuid != $uuid) {
          return abort(404);
        }

        $data['action'] = 'update';
        $data['agama'] = Pegawai::getAgama();
        $data['statusnikah'] = Pegawai::getSatusNikah();
        $data['jk'] = Pegawai::getJenisKelamin();
        $data['jabatan'] = DB::table('kepegawaian.jabatan')
                           ->select('uuid','nama_jabatan')
                           ->where('deleted_at',null)
                           ->get();
        $data['skp'] = DB::table('kepegawaian.status_kepegawaian')
                           ->select('uuid','nama_status_kepegawaian')
                           ->where('deleted_at',null)
                           ->get();
        $data['role'] = DB::table('master.role')
                            ->select('uuid','nama_role')
                            ->where('deleted_at',null)
                            ->get();
        $data['pegawai'] = Pegawai::where('uuid',$uuid)->firstOrFail();
        $data['title'] = 'Profile Pegawai '.$data['pegawai']->nama_pegawai;
        $data['show'] = 'readonly';
        $data['allow-button'] = false;
        return view('AdminLTE.pages.pegawai.form',compact('data'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($uuid)
    {
        //
        $data['action'] = 'update';
        $data['agama'] = Pegawai::getAgama();
        $data['statusnikah'] = Pegawai::getSatusNikah();
        $data['jk'] = Pegawai::getJenisKelamin();
        $data['jabatan'] = DB::table('kepegawaian.jabatan')
                           ->select('uuid','nama_jabatan')
                           ->where('deleted_at',null)
                           ->get();
        $data['skp'] = DB::table('kepegawaian.status_kepegawaian')
                           ->select('uuid','nama_status_kepegawaian')
                           ->where('deleted_at',null)
                           ->get();
        $data['role'] = DB::table('master.role')
                            ->select('uuid','nama_role')
                            ->where('deleted_at',null)
                            ->get();
        $data['pegawai'] = Pegawai::where('uuid',$uuid)->firstOrFail();
        $data['title'] = 'Edit Pegawai '.$data['pegawai']->nama_pegawai;
        $data['readonly'] = 'readonly';
        $data['allow-button'] = true;
        return view('AdminLTE.pages.pegawai.form',compact('data'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $rules = array(
          'nama_pegawai' => 'required',
          'uuid_jabatan' => 'required',
          'tempat_lahir' => 'required',
          'tanggal_lahir' => 'required',
          'agama' => 'required',
          'jk' => 'required',
          'snikah' => 'required',
          'telepon1' => 'required|numeric',
          'telepon2' => 'required|numeric',
          'email' => 'required',
          'alamat' => 'required',
          'statuskep' => 'required',
          // 'foto' => 'required|image'
        );
        $errors = Validator::make(
          $request->all(),
          $rules,
          Validation::ValidationMessage()
        );

        if($errors->fails()) {
          return response()->json([
            'response' => 'failed',
            'header' => 'Gagal',
            'messages' => $errors->errors()->all(),
          ]);
        }

        DB::beginTransaction();

        $pegawai = Pegawai::where('uuid',$request->uuid)->firstOrFail();

        $image_path = '/assets/photos/pegawai/';

        if(!empty($request->foto)) {
          if(!empty($pegawai->foto_pegawai)) {
            Storage::delete($pegawai->foto_pegawai);
          }
          $pegawai->update([
            'foto' => '/'.$request->file('foto')->storeAs($image_path,$request->nip_pegawai.'.'.$request->foto->extension())
          ]);
        }

        $request->request->add([
          'nip_pegawai' => $request->nip_pegawai,
          'nama_pegawai' => $request->nama_pegawai,
          'uuid_jabatan' => $request->uuid_jabatan,
          'tempat_lahir_pegawai' => $request->tempat_lahir,
          'tanggal_lahir_pegawai' => $request->tanggal_lahir,
          'agama_pegawai' => $request->agama,
          'jenis_kelamin_pegawai' => $request->jk,
          'status_nikah_pegawai' => $request->snikah,
          'telepon1_pegawai' => $request->telepon1,
          'telepon2_pegawai' => $request->telepon2,
          'email_pegawai' => $request->email,
          'alamat_pegawai' => $request->alamat,
          'uuid_status' => $request->statuskep,
          //'lama_tahun_bekerja_pegawai' => StatusKepegawaian::where('uuid',$request->statuskep)->firstOrFail()->lama_kerja_status_kepegawaian
        ]);

        $update = $pegawai->update($request->all());

        if($update) {
          $user = User::where('uuid_pegawai',$pegawai->uuid)->first();
          if(!empty($user->uuid)) {
            $user->update([
              'uuid_role' => $request->role
            ]);
          } else if(empty($user->uuid)) {
            User::create([
              'uuid_pegawai' => $pegawai->uuid,
              'username' => $pegawai->nip_pegawai,
              'password' => Hash::make($pegawai->tanggal_lahir_pegawai),
              'uuid_role' => $request->role
            ]);
            $pegawai->update([
              'is_akses' => 1
            ]);
          }
          DB::commit();
          return response()->json([
            'response' => 'success',
            'header' => 'Sukses',
            'messages' => 'Data Sukses Dirubah',
            'post_data' => $request
          ]);
        } else {
          DB::rollback();
          return response()->json([
            'response' => 'failed',
            'header' => 'Gagal',
            'messages' => 'Data Gagal Dirubah',
          ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($uuid)
    {
        //
        $pegawai = Pegawai::where('uuid',$uuid)->firstOrFail();
        $pegawai_photo = $pegawai->foto_pegawai;
        if(!empty($pegawai_photo)) {
          Storage::delete($pegawai_photo);
        }
        $delete = $pegawai->delete();
        if($delete) {
          return response()->json([
            'response' => 'success',
            'header' => 'Sukses',
            'messages' => 'Data Sukses Terhapus'
          ]);
        } else if(!$delete) {
          return response()->json([
            'response' => 'failed',
            'header' => 'Gagal',
            'messages' => 'Data Gagal Terhapus'
          ]);
        }
    }
}
