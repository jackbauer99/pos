<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\StatusKepegawaian;
use App\Models\Validation;
use DB;
use Validator;
use DataTables;

class StatusKepegawaianController extends Controller
{
    //

    public function datatables() {

      $query = StatusKepegawaian::query();
      return Datatables::of($query)
             ->addIndexColumn()
             ->addColumn('action', function($data) {
               $button = '<button type="button" name="update" id="'.route('skp.edit',$data->uuid).'" class="update btn btn-warning btn-sm"><i class = "fa fa-edit"></i></button>';
               $button .= '<button type="button" name="delete" id="'.route('skp.destroy',$data->uuid).'" class="delete btn btn-danger btn-sm"><i class = "fa fa-trash"></i></button> ';
               //$button = 'Ini Button';
               return $button;
             })
             ->rawColumns(['action'])
             ->make(true);
    }

    public function index() {
      return view('AdminLTE.pages.statuskepegawaian.list');
    }

    public function store(Request $request) {

      $rules = array(
        'nama_status_kepegawaian' => 'required',
        'lama_kerja_status_kepegawaian' => 'required|numeric',
        'gaji_pokok_status_kepegawaian' => 'required|numeric'
      );

      $errors = Validator::make(
        $request->all(),
        $rules,
        Validation::ValidationMessage()
      );

      if($errors->fails()) {
        return response()->json([
          'response' => 'failed',
          'header' => 'Gagal',
          'messages' => $errors->errors()->all(),
        ]);
      }

      $request->request->add([
        'nama_status_kepegawaian' => $request->nama_status_kepegawaian,
        'lama_kerja_status_kepegawaian' => $request->lama_kerja_status_kepegawaian,
        'gaji_pokok_status_kepegawaian' => $request->gaji_pokok_status_kepegawaian
      ]);

      $insert = StatusKepegawaian::create($request->all());

      if($insert) {
        return response()->json([
          'response' => 'success',
          'header' => 'Sukses',
          'messages' => 'Data Sukses Tersimpan',
          'post_data' => $request
        ]);
      } else if(!$insert) {
        return response()->json([
          'response' => 'failed',
          'header' => 'Gagal',
          'messages' => 'Data Gagal Tersimpan Silahkan Coba Lagi',
        ]);
      }
    }

    public function edit($uuid) {

      $data = StatusKepegawaian::where('uuid',$uuid)->firstOrFail();
      return response()->json([
        'response' => 'success',
        'data' => $data
      ]);
    }

    public function update(Request $request) {

      $rules = array(
        'nama_status_kepegawaian' => 'required',
        'lama_kerja_status_kepegawaian' => 'required|numeric',
        'gaji_pokok_status_kepegawaian' => 'required|numeric'
      );

      $errors = Validator::make(
        $request->all(),
        $rules,
        Validation::ValidationMessage()
      );

      if($errors->fails()) {
        return response()->json([
          'response' => 'failed',
          'header' => 'Gagal',
          'messages' => $errors->errors()->all(),
        ]);
      }

      $request->request->add([
        'nama_status_kepegawaian' => $request->nama_status_kepegawaian,
        'lama_kerja_status_kepegawaian' => $request->lama_kerja_status_kepegawaian,
        'gaji_pokok_status_kepegawaian' => $request->gaji_pokok_status_kepegawaian
      ]);

      $skp = StatusKepegawaian::where('uuid',$request->uuid)->firstOrFail();

      $update = $skp->update($request->all());

      if($update) {
        return response()->json([
          'response' => 'success',
          'header' => 'Sukses',
          'messages' => 'Data Sukses Dirubah',
          'post_data' => $request
        ]);
      } else if(!$insert) {
        return response()->json([
          'response' => 'failed',
          'header' => 'Gagal',
          'messages' => 'Data Gagal Dirubah Silahkan Coba Lagi',
        ]);
      }
    }

    public function destroy($uuid) {

      $skp = StatusKepegawaian::where('uuid',$uuid)->firstOrFail();
      $delete = $skp->delete();

      if($delete) {
        return response()->json([
          'response' => 'success',
          'header' => 'Sukses',
          'messages' => 'Data Sukses Terhapus'
        ]);
      } else if(!$delete) {
        return response()->json([
          'response' => 'failed',
          'header' => 'Gagal',
          'messages' => 'Data Gagal Terhapus'
        ]);
      }
    }
}
