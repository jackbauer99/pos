<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use App\Models\Pegawai;
use App\Models\Role;
use App\Models\Validation;
use App\Helper\LogHelper;
use Validator;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use App\Exceptions\Handler;

class AuthController extends Controller
{
    //

    public static function index() {

      $data['title'] = 'Login';
      return view('Login.login',compact('data'));
    }

    public static function getLogin(Request $request) {

      $rules = array(
        'username' => 'required',
        'password' => 'required'
      );
      $errors = Validator::make(
          $request->all(),
          $rules,
          Validation::ValidationMessage()
      );

      if($errors->fails()) {
        return response()->json([
          'response' => 'failed',
          'header' => 'Gagal',
          'messages' => $errors->errors()->all(),
        ]);
      }

        $login = User::where('username',$request->username)->first();

        if(!$login) {
          return response()->json([
            'response' => 'failed',
            'header' => 'Username Tidak Ditemukan',
            'messages' => 'Username Tidak Ditemukan Coba Ulangi'
          ]);
        }

        if(!Hash::check($request->password,$login->password)) {
          return response()->json([
            'response' => 'failed',
            'header' => 'Password Salah',
            'messages' => 'Password Salah Coba Ulangi'
          ]);
        }

        // if($login->is_login == 1) {
        //   return response()->json([
        //     'response' => 'failed',
        //     'header' => 'Akun Sudah Login',
        //     'messages' => 'Silahkan Logout Terlebih Dahulu'
        //   ]);
        // }

        $checklogin = Auth::login($login);

        // $post = array();
        //
        // $post['_token'] = $request->_token;
        // $post['username'] = $request->username;
        // $post['password'] = str_repeat("*", strlen($request->password));
        //
        // $action = json_encode($post);
        //
        //
        // $log = LogHelper::BeginLog(Auth::user()->pegawai->uuid,'Login',$action,'50','Success');
        //
        // if(!$log) {
        //   Auth::logout();
        // }

        if($checklogin == null) {

          $update = $login->update([
            'is_login' => 1
          ]);

          if($update) {
            return response()->json([
              'response' => 'success',
              'header' => 'Login Sukses',
              'messages' => 'Sukses Login, Anda Akan Diarahkan Menuju Halaman dalam 3 detik',
              'url' => 'barang.index'
            ]);
          } else if(!$update) {
            return response()->json([
              'response' => 'failed',
              'header' => 'Gagal Login',
              'messages' => 'Gagal Login',
            ]);
          }

        } else if($checklogin != null) {
            return response()->json([
              'response' => 'failed',
              'header' => 'Gagal Login',
              'messages' => 'Gagal Login',
            ]);
        }
      }

      public function UserRequestLogin($uuid) {

        $pegawai = Pegawai::where('uuid',$uuid)->firstOrFail();
        $insert = User::create([
          'uuid_pegawai' => $uuid,
          'username' => $pegawai->nip_pegawai,
          'password' => Hash::make($pegawai->tanggal_lahir_pegawai),
          'uuid_role' => Role::find(2)->uuid
        ]);
        if($insert) {
          Pegawai::where('uuid',$uuid)->first()->update([
            'is_akses' => 1
          ]);
          return response()->json([
            'response' => 'success',
            'header' => 'Sukses',
            'messages' => 'Penamabahan Akses User Berhasil'
          ]);
        } else if(!$insert) {
          return response()->json([
            'response' => 'failed',
            'header' => 'Gagal',
            'messages' => 'Penamabahan Akses User Gagal'
          ]);
        }
      }

      public function logout($uuid) {

        $user = User::where('uuid',$uuid)->first();
        $update = $user->update([
          'is_login' => 0
        ]);
        if($update) {
            Auth::logout();
            return response()->json([
              'response' => 'success',
              'header' => 'Logout',
              'messages' => 'Anda Sudah Keluar, anda akan diarahkan ke halaman Login selama 3 detik'
            ]);
        } else if(!$update) {
          return response()->json([
            'response' => 'gagal',
            'header' => 'Gagal Logout',
            'messages' => 'Gagal Logout Sistem Sedang Sibuk'
          ]);
        }



      }
}
