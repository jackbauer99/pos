<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\KategoriBarang;
use App\Models\Validation;
use DataTables;
use Validator;
use DB;

class KategoriController extends Controller
{

    public function datatables() {
      $query = KategoriBarang::query();
      return Datatables::of($query)
             ->addIndexColumn()
             ->addColumn('action', function($data) {
               $button = '<button type="button" name="delete" id="'.route('kategori.destroy',$data->uuid).'" class="delete btn btn-danger btn-sm"><i class = "fa fa-trash"></i></button>';
               return $button;
             })
             ->rawColumns(['action'])
             ->make(true);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('AdminLTE.pages.kategori.list');

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //

    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = array(
          'kategori_nama' => 'required'
        );
        $errors = Validator::make(
          $request->all(),
          $rules,
          Validation::ValidationMessage(),
        );

        if($errors->fails()) {
          return response()->json([
            'response' => 'failed',
            'header' => 'Gagal',
            'messages' => $errors->errors()->all(),
          ]);
        }

        $request->request->add([
          'nama_kategori' => $request->kategori_nama
        ]);

        $insert = KategoriBarang::create($request->all());

        if($insert) {
          return response()->json([
            'response' => 'success',
            'header' => 'Sukses',
            'messages' => 'Data Sukses Tersimpan',
            'post_data' => $request
          ]);
        } else if(!$insert) {
          return response()->json([
            'response' => 'failed',
            'header' => 'Gagal',
            'messages' => 'Data Gagal Tersimpan Silahkan Coba Lagi',
          ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($uuid)
    {
        //
        $data = KategoriBarang::where('uuid',$uuid)->firstOrFail();
        $delete = $data->delete();
        if($delete) {
          return response()->json([
            'response' => 'success',
            'header' => 'Sukses',
            'messages' => 'Data Sukses Terhapus'
          ]);
        } else if(!$delete) {
          return response()->json([
            'response' => 'failed',
            'header' => 'Gagal',
            'messages' => 'Data Gagal Terhapus'
          ]);
        }
    }
}
