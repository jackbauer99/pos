<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Jabatan;
use App\Models\Validation;
use DB;
use DataTables;
use Validator;

class JabatanController extends Controller
{

    public function datatables() {
      $query = Jabatan::query();
      return Datatables::of($query)
             ->addIndexColumn()
             ->addColumn('action', function($data) {
               $button = '<button type="button" name="delete" id="'.route('jabatan.destroy',$data->uuid).'" class="delete btn btn-danger btn-sm"><i class = "fa fa-trash"></i></button> ';
               $button .= '<button type="button" name="update" id="'.route('jabatan.edit',$data->uuid).'" class="update btn btn-warning btn-sm"><i class = "fa fa-edit"></i></button>';
               return $button;
             })
             ->rawColumns(['action'])
             ->make(true);
    }
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        return view('AdminLTE.pages.jabatan.list');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = array(
          'kode_jabatan' => 'required|min:15',
          'nama_jabatan' => 'required'
        );
        $errors = Validator::make(
          $request->all(),
          $rules,
          Validation::ValidationMessage()
        );

        if($errors->fails()) {
          return response()->json([
            'response' => 'failed',
            'header' => 'Gagal',
            'messages' => $errors->errors()->all(),
          ]);
        }

        $request->request->add([
          'kode_jabatan' => $request->kode_jabatan,
          'nama_jabatan' => $request->nama_jabatan
        ]);

        $insert = Jabatan::create($request->all());

        if($insert) {
          return response()->json([
            'response' => 'success',
            'header' => 'Sukses',
            'messages' => 'Data Sukses Tersimpan',
            'post_data' => $request
          ]);
        } else if(!$insert) {
          return response()->json([
            'response' => 'failed',
            'header' => 'Gagal',
            'messages' => 'Data Gagal Tersimpan Silahkan Coba Lagi',
          ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($uuid)
    {
        //
        $data = Jabatan::where('uuid',$uuid)->first();
        return response()->json([
          'response' => 'success',
          'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        $rules = array(
          'kode_jabatan' => 'required|min:15',
          'nama_jabatan' => 'required'
        );
        $errors = Validator::make(
          $request->all(),
          $rules,
          Validation::ValidationMessage()
        );

        if($errors->fails()) {
          return response()->json([
            'response' => 'failed',
            'header' => 'Gagal',
            'messages' => $errors->errors()->all(),
          ]);
        }

        $request->request->add([
          'kode_jabatan' => $request->kode_jabatan,
          'nama_jabatan' => $request->nama_jabatan
        ]);

        $jabatan = Jabatan::where('uuid',$request->uuid)->first();

        $update = $jabatan->update($request->all());

        if($update) {
          return response()->json([
            'response' => 'success',
            'header' => 'Sukses',
            'messages' => 'Data Sukses Dirubah',
            'post_data' => $request
          ]);
        } else if(!$insert) {
          return response()->json([
            'response' => 'failed',
            'header' => 'Gagal',
            'messages' => 'Data Gagal Dirubah Silahkan Coba Lagi',
          ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($uuid)
    {
        //
        $data = Jabatan::where('uuid',$uuid)->firstOrFail();
        $delete = $data->delete();
        if($delete) {
          return response()->json([
            'response' => 'success',
            'header' => 'Sukses',
            'messages' => 'Data Sukses Terhapus'
          ]);
        } else if(!$delete) {
          return response()->json([
            'response' => 'failed',
            'header' => 'Gagal',
            'messages' => 'Data Gagal Terhapus'
          ]);
        }
    }
}
