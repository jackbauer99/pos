<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Barang;
use App\Models\KategoriBarang;
use App\Models\Validation;
use DB;
use DataTables;
use Validator;

class BarangController extends Controller
{

    public function datatables() {
      $query = Barang::with([
        'KategoriBarang'
      ])->select('barang.*');
      return Datatables::of($query)
              ->addIndexColumn()
              ->addColumn('action', function($data) {
                 $button = '<button type="button" name="delete" id="'.route('barang.destroy',$data->uuid).'" class="delete btn btn-danger btn-sm"><i class = "fa fa-trash"></i></button> ';
                 $button .= '<button type="button" name="update" id="'.route('barang.edit',$data->uuid).'" class="update btn btn-warning btn-sm"><i class = "fa fa-edit"></i></button>';
                 return $button;
               })
              ->rawColumns(['action'])
              ->make(true);
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        $data['kategori'] = DB::table('master.kategori_barang')
                            ->select('uuid','nama_kategori')
                            ->where('deleted_at',null)
                            ->get();
        return view('AdminLTE.pages.barang.list',compact('data'));
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
        $rules = array(
          'kode_barang' => 'required',
          'nama_barang' => 'required',
          'uuid_kategori' => 'required',
          'manufaktur_barang' => 'required',
          'jumlah_stok_barang' => 'required',
          'harga_per_satuan_barang' => 'required'
        );
        $errors = Validator::make(
          $request->all(),
          $rules,
          Validation::ValidationMessage(),
        );

        if($errors->fails()) {
          return response()->json([
            'response' => 'failed',
            'header' => 'Gagal',
            'messages' => $errors->errors()->all(),
          ]);
        }

        DB::beginTransaction();

        $request->request->add([
          'kode_barang' => $request->kode_barang,
          'nama_barang' => $request->nama_barang,
          'uuid_kategori' => $request->uuid_kategori,
          'manufaktur_barang' => $request->manufaktur_barang,
          'jumlah_stok_barang' => $request->jumlah_stok_barang,
          'harga_per_satuan_barang' => $request->harga_per_satuan_barang
        ]);

        $insert = Barang::create($request->all());

        if($insert) {
          DB::commit();
          return response()->json([
            'response' => 'success',
            'header' => 'Sukses',
            'messages' => 'Data Sukses Tersimpan',
            'post_data' => $request
          ]);
        } else {
          DB::rollback();
          return response()->json([
            'response' => 'failed',
            'header' => 'Gagal',
            'messages' => 'Data Gagal Tersimpan',
          ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($uuid)
    {
        //
        $data = Barang::where('uuid',$uuid)->firstOrFail();
        return response()->json([
          'response' => 'success',
          'data' => $data
        ]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
        //
        //
        $rules = array(
          'kode_barang' => 'required',
          'nama_barang' => 'required',
          'uuid_kategori' => 'required',
          'manufaktur_barang' => 'required',
          'jumlah_stok_barang' => 'required',
          'harga_per_satuan_barang' => 'required'
        );
        $errors = Validator::make(
          $request->all(),
          $rules,
          Validation::ValidationMessage(),
        );

        if($errors->fails()) {
          return response()->json([
            'response' => 'failed',
            'header' => 'Gagal',
            'messages' => $errors->errors()->all(),
          ]);
        }

        DB::beginTransaction();

        $request->request->add([
          'kode_barang' => $request->kode_barang,
          'nama_barang' => $request->nama_barang,
          'uuid_kategori' => $request->uuid_kategori,
          'manufaktur_barang' => $request->manufaktur_barang,
          'jumlah_stok_barang' => $request->jumlah_stok_barang,
          'harga_per_satuan_barang' => $request->harga_per_satuan_barang
        ]);

        $barang = Barang::where('uuid',$request->uuid)->firstOrFail();
        $update = $barang->update($request->all());

        if($update) {
          DB::commit();
          return response()->json([
            'response' => 'success',
            'header' => 'Sukses',
            'messages' => 'Data Sukses Dirubah',
            'post_data' => $request
          ]);
        } else {
          DB::rollback();
          return response()->json([
            'response' => 'failed',
            'header' => 'Gagal',
            'messages' => 'Data Gagal Tersimpan',
          ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($uuid)
    {
        //
        $data = Barang::where('uuid',$uuid)->firstOrFail();
        $delete = $data->delete();
        if($delete) {
          return response()->json([
            'response' => 'success',
            'header' => 'Sukses',
            'messages' => 'Data Sukses Terhapus'
          ]);
        } else if(!$delete) {
          return response()->json([
            'response' => 'failed',
            'header' => 'Gagal',
            'messages' => 'Data Gagal Terhapus'
          ]);
        }
    }
}
