<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Log;
// use App\Helper\Log;
use App\Models\Pegawai;
use App\Models\User;
use App\Helper\Date;
use DB;
use DataTables;
use Validator;
use Storage;
use Hash;


class MainController extends Controller
{
    //

    public function datatables() {

      $query = Log::with([
        'User',
        'Pegawai'
      ])->select('logs.*');

      return Datatables::of($query)
             ->addIndexColumn()
             ->addColumn('action', function($data) {
                 $button = '<button type="button" name="view" id="#" class="view btn btn-xs btn-primary" data-toggle="tooltip" title="Lihat Data"><i class = "fa fa-eye"></i></button>';
                 $button .= '<button type="button" name="delete" id="#" class="delete btn btn-danger btn-xs" data-toggle="tooltip" title="Hapus"><i class = "fa fa-trash"></i></button> ';
                 $button .= '<button type="button" name="update" id="#" class="update btn btn-warning btn-xs" data-toggle="tooltip" title="Edit"><i class = "fa fa-edit"></i></button>';
                 return $button;
            })     
             ->rawColumns(['action'])
             ->make(true);
    }

    public function index() {

      $data['pegawai'] = Pegawai::count();
      $data['user'] = User::count();
      return view('AdminLTE.pages.dashboard',compact('data'));

    }
}
