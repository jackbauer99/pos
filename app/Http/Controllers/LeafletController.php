<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class LeafletController extends Controller
{
    //
    public function index() {
      return view('AdminLTE.pages.leaflet.maps');
    }
}
