<?php

namespace App\Helper;

class Date {

  public static function getRandomDateBirth() {
    $tahun_lahir = rand(1950,2000);
    $bulan_lahir = rand(1,12);
    if($bulan_lahir == 1 || $bulan_lahir == 3 || $bulan_lahir == 5 || $bulan_lahir == 7 || $bulan_lahir == 8 || $bulan_lahir == 10
    || $bulan_lahir == 12) {
      $tanggal_lahir = rand(1,31);
    } else if($bulan_lahir == 4 || $bulan_lahir == 6 || $bulan_lahir == 9 || $bulan_lahir == 11) {
      $tanggal_lahir = rand(1,30);
    } else if($bulan_lahir == 2) {
      if($tahun_lahir % 4 == 0) {
        $tanggal_lahir = rand(1,29);
      } else if($tahun_lahir % 4 != 0) {
        $tanggal_lahir = rand(1,28);
      }
    }

    return $tahun_lahir.'-'.$bulan_lahir.'-'.$tanggal_lahir;

  }
}

 ?>
