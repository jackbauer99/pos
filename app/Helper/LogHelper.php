<?php

/*
@author nugraha
*/

namespace App\Helper;

use App\Models\Log;
use Jenssegers\Agent\Agent;
use Stevebauman\Location\Location;

class LogHelper {

  public static function BeginLog($user,$activity,$action,$execution,$status) {

    $agent = new Agent();
    $location = new Location();
    date_default_timezone_set("Asia/Jakarta");
    if($agent->isMobile()) {
      $device = $agent->device();
    } else if($agent->isTablet()) {
      $device = $agent->device();
    } else if($agent->isDesktop()) {
      $device = $agent->device();
    } else if($agent->isPhone()) {
      $device = $agent->device();
    }
    $log  = Log::create([
      'uuid_user' => $user,
      'activity' => $activity,
      'user_ipaddress' => $_SERVER['REMOTE_ADDR'],
      'user_device' => $agent->device().' - '.$agent->platform(),
      'user_browser' => $agent->browser(),
      'user_location' => $location->get($_SERVER['REMOTE_ADDR']),
      'last_login' => date('Y-m-d H:i:s'),
      'action_data' => $action,
      'execution_time' => $execution,
      'status' => $status
    ]);

    if($log) {
      return true;
    } else {
      return false;
    }

  }

}


 ?>
