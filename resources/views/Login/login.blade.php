<!DOCTYPE html>
<html lang="en">
<head>
  <meta name="csrf-token" content="{{ csrf_token() }}">
	<title>{{ $data['title'] }}</title>
	<meta charset="UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
<!--===============================================================================================-->
	<link rel="icon" type="image/png" href="{{ url('assets/login') }}/images/icons/favicon.ico"/>
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ url('assets/login') }}/vendor/bootstrap/css/bootstrap.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ url('assets/login') }}/fonts/font-awesome-4.7.0/css/font-awesome.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ url('assets/login') }}/fonts/Linearicons-Free-v1.0.0/icon-font.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ url('assets/login') }}/vendor/animate/animate.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ url('assets/login') }}/vendor/css-hamburgers/hamburgers.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ url('assets/login') }}/vendor/animsition/css/animsition.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ url('assets/login') }}/vendor/select2/select2.min.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ url('assets/login') }}/vendor/daterangepicker/daterangepicker.css">
<!--===============================================================================================-->
	<link rel="stylesheet" type="text/css" href="{{ url('assets/login') }}/css/util.css">
	<link rel="stylesheet" type="text/css" href="{{ url('assets/login') }}/css/main.css">
<!--===============================================================================================-->
<!-- Font Awesome -->
<link rel="stylesheet" href="{{ url('assets/AdminLTE') }}/plugins/fontawesome-free/css/all.min.css">
<!-- SweetAlert2 -->
  <link rel="stylesheet" href="{{ url('assets/AdminLTE') }}/plugins/sweetalert2-v2/dist/sweetalert2.min.css">
</head>
<body style="background-color: #666666;">

	<div class="limiter">
		<div class="container-login100">
			<div class="wrap-login100">
				<form class="login100-form" id = "loginform" method="post" enctype="multipart/form-data">

          @csrf

					<span class="login100-form-title p-b-43">
						Silahkan Login
					</span>


					<div class="wrap-input100" data-validate = "Valid email is required: ex@abc.xyz">
						<input class="input100" id="username" type="text" name="username" maxlength="15">
						<span class="focus-input100"></span>
						<span class="label-input100">Username</span>
					</div>


					<div class="wrap-input100" data-validate="Password is required">
						<input class="input100" id = "password" type="password" name="password">
						<span class="focus-input100"></span>
						<span class="label-input100">Password</span>
					</div>

					<div class="flex-sb-m w-full p-t-3 p-b-32">
						<div class="contact100-form-checkbox">
							<input class="input-checkbox100" id="ckb1" type="checkbox" name="remember-me">
							<label class="label-checkbox100" for="ckb1">
								Remember me
							</label>
						</div>

						<div>
							<a href="#" class="txt1">
								Forgot Password?
							</a>
						</div>
					</div>


					<div class="container-login100-form-btn">
						<button type="submit" class="login100-form-btn" id = "login">
							Login
						</button>
					</div>

				</form>

				<div class="login100-more" style="background-image: url('{{ url("assets/login") }}/images/bg-01.jpg');">
				</div>
			</div>
		</div>
	</div>





  <!-- jQuery -->
  <script src="{{ url('assets/AdminLTE') }}/plugins/jquery/jquery.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="{{ url('assets/AdminLTE') }}/plugins/jquery-ui/jquery-ui.min.js"></script>
<!--===============================================================================================-->
	<script src="{{ url('assets/login') }}/vendor/animsition/js/animsition.min.js"></script>
<!--===============================================================================================-->
	<script src="{{ url('assets/login') }}/vendor/bootstrap/js/popper.js"></script>
	<script src="{{ url('assets/login') }}/vendor/bootstrap/js/bootstrap.min.js"></script>
<!--===============================================================================================-->
	<script src="{{ url('assets/login') }}/vendor/select2/select2.min.js"></script>
<!--===============================================================================================-->
	<script src="{{ url('assets/login') }}/vendor/daterangepicker/moment.min.js"></script>
	<script src="{{ url('assets/login') }}/vendor/daterangepicker/daterangepicker.js"></script>
<!--===============================================================================================-->
	<script src="{{ url('assets/login') }}/vendor/countdowntime/countdowntime.js"></script>
<!--===============================================================================================-->
	<!-- <script src="{{ url('assets/login') }}/js/main.js"></script> -->
  <script src="{{ url('assets/AdminLTE') }}/plugins/blockUI/jquery.blockUI.js"></script>
  <!-- SweetAlert2 -->
  <script src="{{ url('assets/AdminLTE') }}/plugins/sweetalert2-v2/dist/sweetalert2.min.js"></script>

  <script>
  $.ajaxSetup({
      headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
      }
  });
  $(document).ready(function(){
    $('#loginform').on('submit',function(){
      event.preventDefault();
      let data = new FormData($(this)[0]);
      $('#login').html('<i class="fas fa-spinner fa-pulse"></i>');
      $.ajax({
        url: '{{ route("auth.login") }}',
        method: "POST",
        data: data,
        contentType: false,
        cache: false,
        processData: false,
        dataType: "json",
        success: function(data) {
          $('#login').html('Login');
          if(data.response == 'success') {
            Swal.fire({
              type: 'success',
              title: data.header,
              text: data.messages,
              timer: 3000,
              icon: 'success',
              showCancelButton: false,
              showConfirmButton: false
            }).then (function() {
                window.location.href = "{{ route('dashboard.index') }}";
            });
          } else if(data.response == 'failed') {

            if($.isArray(data.messages)){
                var messages = data.messages.join('<br/>');
            } else {
              var messages = data.messages;
            }
            //console.log(messages)
            Swal.fire(
              data.header,
              messages,
              'error'
            )
          }
        },
        error: function(data) {
          //console.log(data)
          Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Error',
            icon: 'error'
          });
            $('#login').html('Login');
        }
      })
    });
  });

  </script>

</body>
</html>
