@extends('AdminLTE.app')

@section('content')

<style>
#maps { height: 500px; }
</style>

<div class="row">
  <div class="col-md-12">
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Tambah Kategori</h3>
      </div>
        <div id = "maps"></div>
      </div>
    </div>
  </div>

@push('script')
<script>
$(document).ready(function() {

  var mymap = L.map('maps');

  L.tileLayer('https://api.mapbox.com/styles/v1/{id}/tiles/{z}/{x}/{y}?access_token={accessToken}', {
    attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
    maxZoom: 18,
    id: 'mapbox/streets-v11',
    tileSize: 512,
    zoomOffset: -1,
    accessToken: 'pk.eyJ1IjoibnU5OWV0eiIsImEiOiJja2cwOXdiaHkwc2VhMnFyMDh0dzN6amh2In0.8UMoP_3ozYme9HN4mHsl5g'
  }).addTo(mymap);

  //var marker = L.marker([-7.2697333,112.7852863]).addTo(mymap);

  L.Routing.control({
    waypoints: [
        L.latLng(-7.2755422,112.7674182),
        //L.latLng(-7.2697333,112.7852863),
        L.latLng(-7.2680224,112.7574049)
    ],
    routeWhileDragging: true
}).addTo(mymap);

});

</script>
@endpush

@endsection
