@extends('AdminLTE.app')

@section('content')

<style>

#foto {
  display: none;
}

</style>

<div class="row">
  <div class="col-md-12">
    <div class="row">
      <div class="col-md-12">
        <!-- <button type="button" id = "reset1" class="btn-sm btn-warning float-right"><i class="fa fa-redo"></i> Reset</button> -->
        <button type="button" id = "back" class="btn btn-sm btn-danger float-right"><i class="fa fa-chevron-left"></i> Kembali</button>
      </div>
    </div>
    <br/>
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">{{ $data['title'] }}</h3>
      </div>
        <form id="pegawai_form" method="post" enctype="multipart/form-data">
          @csrf
          <div class="card-body">
            <div class="form-group">
              <div class="row">
                <div class="col-md-3">
                </div>
                <div class="col-md-6">
                  @if(!empty($data['pegawai']->foto_pegawai))
                    <center><img src="{{ url('/').$data['pegawai']->foto_pegawai ?? '' }}" id="preview" class="img-circle elevation-2" width="200" height="200" alt=""></center>
                  @elseif(empty($data['pegawai']->foto_pegawai))
                    <center><img src="{{ url('/').'/assets/photos/default.png' }}" id="preview" class="img-circle elevation-2" width="200" height="200" alt=""></center>
                  @endif
                </div>
                <div class="col-md-3">
                </div>
              </div>
              <br/>
              <div class="row">
                <!-- <div class="col-md-6"> -->
                  <label for="NIP" class="col-md-2">NIP</label>
                  <div class="col-md-4" style="display:block;word-wrap:break-word;">
                    <input type="text" class="form-control form-control-sm" id="nip_pegawai" name="nip_pegawai" maxlength="15" value="{{ $data['pegawai']->nip_pegawai ?? '' }}" {{ $data['readonly'] ?? ''}} {{ $data['show'] ?? ''}}>
                  </div>
                  <label for="nama" class="col-md-2">Nama Pegawai</label>
                  <div class="col-md-4" style="display:block;word-wrap:break-word;">
                    <input type="text" class="form-control form-control-sm" id="nama_pegawai" name="nama_pegawai" value="{{ $data['pegawai']->nama_pegawai ?? ''}}" {{ $data['show'] ?? ''}}>
                  </div>
                <!-- </div> -->
              </div>
              <br/>
              <div class="row">
                <!-- <div class="col-md-6"> -->
                  <label for="jabatan" class="col-md-2">Jabatan</label>
                  <div class="col-md-4" style="display:block;word-wrap:break-word;">
                    <select class="form-control form-control-sm select2" style="width: 100%;" id= "uuid_jabatan" name="uuid_jabatan" {{ $data['show'] ?? ''}}>
                      <option></option>
                      @foreach($data['jabatan'] as $jabatan)
                        @if(!empty($data['pegawai']->uuid_jabatan))
                          @if($data['pegawai']->uuid_jabatan == $jabatan->uuid)
                            <option value="{{ $jabatan->uuid }}" selected>{{ $jabatan->nama_jabatan }}</option>
                          @elseif($data['pegawai']->uuid_jabatan != $jabatan->uuid)
                            <option value="{{ $jabatan->uuid }}">{{ $jabatan->nama_jabatan }}</option>
                          @endif
                        @elseif(empty($data['pegawai']->uuid_jabatan))
                          <option value="{{ $jabatan->uuid }}">{{ $jabatan->nama_jabatan }}</option>
                        @endif
                      @endforeach
                    </select>
                  </div>
                  <label for="tempat_lahir" class="col-md-2">Tempat Lahir</label>
                  <div class="col-md-4">
                    <input type="text" class="form-control form-control-sm" id="tempat_lahir" name="tempat_lahir" value="{{ $data['pegawai']->tempat_lahir_pegawai ?? ''}}" {{ $data['show'] ?? ''}}>
                  </div>
                <!-- </div> -->
              </div>
              <br/>
              <div class="row">
                <!-- <div class="col-md-6"> -->
                <label for="tanggal_lahir" class="col-md-2">Tanggal Lahir</label>
                <div class="col-md-4">
                  <input type="text" class="form-control form-control-sm datepicker" id="tanggal_lahir" name="tanggal_lahir" value="{{ $data['pegawai']->tanggal_lahir_pegawai ?? ''}}" {{ $data['show'] ?? ''}}>
                </div>
                <label for="agama" class="col-md-2">Agama</label>
                <div class="col-md-4" style="display:block;word-wrap:break-word;">
                  <select class="form-control form-control-sm select2" style="width: 100%;" id= "agama" name="agama" {{ $data['show'] ?? ''}}>
                    <option></option>
                    @foreach($data['agama'] as $key => $value)
                      @if(!empty($data['pegawai']->agama_pegawai))
                        @if($data['pegawai']->agama_pegawai == $key)
                          <option value="{{ $key }}" selected>{{ $data['agama'][$key] }}</option>
                        @elseif($data['pegawai']->agama_pegawai != $key)
                          <option value="{{ $key }}">{{ $data['agama'][$key] }}</option>
                        @endif
                      @elseif(empty($data['pegawai']->agama_pegawai))
                          <option value="{{ $key }}">{{ $data['agama'][$key] }}</option>
                      @endif
                    @endforeach
                  </select>
                </div>
                <!-- </div> -->
              </div>
              <br/>
              <div class="row">
                <!-- <div class="col-md-6"> -->
                <label for="jk" class="col-md-2">Jenis Kelamin</label>
                <div class="col-md-4" style="display:block;word-wrap:break-word;">
                  <select class="form-control form-control-sm select2" style="width: 100%;" id= "jk" name="jk" {{ $data['show'] ?? ''}}>
                    <option></option>
                    @foreach($data['jk'] as $key => $value)
                      @if(!empty($data['pegawai']->jenis_kelamin_pegawai))
                        @if($data['pegawai']->jenis_kelamin_pegawai == $key)
                          <option value="{{ $key }}" selected>{{ $data['jk'][$key] }}</option>
                        @elseif($data['pegawai']->jenis_kelamin_pegawai != $key)
                          <option value="{{ $key }}">{{ $data['jk'][$key] }}</option>
                        @endif
                      @elseif(empty($data['pegawai']->jenis_kelamin_pegawai))
                          <option value="{{ $key }}">{{ $data['jk'][$key] }}</option>
                      @endif
                    @endforeach
                  </select>
                </div>
                <label for="statusnikah" class="col-md-2">Status Nikah</label>
                <div class="col-md-4" style="display:block;word-wrap:break-word;">
                  <select class="form-control form-control-sm select2" style="width: 100%;" id= "snikah" name="snikah" {{ $data['show'] ?? ''}}>
                    <option></option>
                    @foreach($data['statusnikah'] as $key => $value)
                      @if(!empty($data['pegawai']->status_nikah_pegawai))
                        @if($data['pegawai']->status_nikah_pegawai == $key)
                          <option value="{{ $key }}" selected>{{ $data['statusnikah'][$key] }}</option>
                        @elseif($data['pegawai']->status_nikah_pegawai != $key)
                          <option value="{{ $key }}">{{ $data['statusnikah'][$key] }}</option>
                        @endif
                      @elseif(empty($data['pegawai']->status_nikah_pegawai))
                          <option value="{{ $key }}">{{ $data['statusnikah'][$key] }}</option>
                      @endif
                    @endforeach
                  </select>
                </div>
                <!-- </div> -->
              </div>
              <br/>
              <div class="row">
                <!-- <div class="col-md-6"> -->
                  <label for="telepon1" class="col-md-2">Telepon 1</label>
                  <div class="col-md-4" style="display:block;word-wrap:break-word;">
                    <input type="text" class="form-control form-control-sm" id="telepon1" name="telepon1" maxlength="15" value="{{ $data['pegawai']->telepon1_pegawai ?? ''}}" {{ $data['show'] ?? ''}}>
                  </div>
                  <label for="telepon2" class="col-md-2">Telepon 2</label>
                  <div class="col-md-4" style="display:block;word-wrap:break-word;">
                    <input type="text" class="form-control form-control-sm" id="telepon2" name="telepon2" maxlength="15" value="{{ $data['pegawai']->telepon2_pegawai ?? ''}}" {{ $data['show'] ?? ''}}>
                  </div>
                <!-- </div> -->
              </div>
              <br/>
              <div class="row">
                <!-- <div class="col-md-6"> -->
                  <label for="email" class="col-md-2">Email</label>
                  <div class="col-md-4" style="display:block;word-wrap:break-word;">
                    <input type="email" class="form-control form-control-sm" id="email" name="email" value="{{ $data['pegawai']->email_pegawai ?? ''}}" {{ $data['show'] ?? ''}}>
                  </div>
                  <label for="alamat" class="col-md-2">Alamat</label>
                  <div class="col-md-4" style="display:block;word-wrap:break-word;">
                    <textarea class="form-control form-control-sm" id="alamat" name="alamat" rows="3" {{ $data['show'] ?? ''}}>{{ $data['pegawai']->alamat_pegawai ?? ''}}</textarea>
                  </div>
                <!-- </div> -->
              </div>
              <br/>
              <div class="row">
                <!-- <div class="col-md-6"> -->
                <label for="statuskep" class="col-md-2">Status Kepegawaian</label>
                <div class="col-md-4" style="display:block;word-wrap:break-word;">
                  <select class="form-control form-control-sm select2" style="width: 100%;" id= "statuskep" name="statuskep" {{ $data['show'] ?? ''}}>
                    <option></option>
                    @foreach($data['skp'] as $skp)
                      @if(!empty($data['pegawai']->uuid_status))
                        @if($data['pegawai']->uuid_status == $skp->uuid)
                          <option value="{{ $skp->uuid }}" selected>{{ $skp->nama_status_kepegawaian }}</option>
                        @elseif($data['pegawai']->uuid_status != $skp->uuid)
                          <option value="{{ $skp->uuid }}">{{ $skp->nama_status_kepegawaian }}</option>
                        @endif
                      @elseif(empty($data['pegawai']->uuid_status))
                          <option value="{{ $skp->uuid }}">{{ $skp->nama_status_kepegawaian }}</option>
                      @endif
                    @endforeach
                  </select>
                </div>
                <label for="foto" class="col-md-2">Akses Login</label>
                <div class="col-md-4" style="display:block;word-wrap:break-word;">
                  <select class="form-control form-control-sm select2" style="width: 100%;" id= "role" name="role" {{ $data['show'] ?? ''}}>
                    <option></option>
                    @foreach($data['role'] as $role)
                      @if(!empty($data['pegawai']->user->uuid_role))
                        @if($data['pegawai']->user->uuid_role == $role->uuid)
                          <option value="{{ $role->uuid }}" selected>{{ $role->nama_role }}</option>
                        @elseif($data['pegawai']->user->uuid_role != $role->uuid)
                          <option value="{{ $role->uuid }}">{{ $role->nama_role }}</option>
                        @endif
                      @elseif(empty($data['pegawai']->user->uuid_role))
                          <option value="{{ $role->uuid }}">{{ $role->nama_role }}</option>
                      @endif
                    @endforeach
                  </select>
                </div>
                <!-- </div> -->
              </div>
              <br/>
            <div class="row">
              <div class="col-4">
              </div>
              <div class="col-8">
                <input type="file" name="foto" accept="image/*" id = "foto">
                @if($data['allow-button'] && Auth::user()->role->id == 1)
                  <input type = "hidden" id = "aksi" value="{{ $data['action'] }}">
                  <input type = "hidden" id = "uuid" name="uuid" value="{{ $data['pegawai']->uuid ?? '' }}">
                  <div class="col-sm-2 btn-group float-right">
                    <button type="button" id = "reset1" class="btn btn-warning"><i class="fa fa-redo"></i> Reset</button>
                  </div>
                  <div class="col-sm-3 btn-group float-right">
                    <button type="submit" id = "save" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                  </div>
                @elseif(!$data['allow-button'] && Auth::user()->role->id == 1)
                  <div class="col-sm-2 btn-group float-right">
                    <button type = "button" class="btn btn-warning" id = "ubah" value="{{ route('pegawai.edit',$data['pegawai']->uuid) }}"><i class="fa fa-edit"></i> Ubah</button>
                  </div>
                  <div class="col-sm-2 btn-group float-right">
                    <button type = "button" class="btn btn-danger" id = "hapus" value="{{ $data['pegawai']->uuid }}"><i class="fa fa-trash"></i> Hapus</button>
                  </div>
                @endif
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

@push('script')
<script>
$(document).ready(function(){

  bsCustomFileInput.init();

  //$('[name=tanggal_lahir]').attr('placeholder','YYYY/MM/DD');
  $('#tanggal_lahir').daterangepicker({
      singleDatePicker: true,
      showDropdowns: true,
      autoApply: true,
      locale: {
        format: 'YYYY/MM/DD',
      }
  });

  $('#ubah').click(function(){
      window.location.href = $('#ubah').val();
  });

  @if(!empty($data['show']))
    $('#uuid_jabatan').prop('disabled',true);
    $('#agama').prop('disabled',true);
    $('#jk').prop('disabled',true);
    $('#snikah').prop('disabled',true);
    $('#role').prop('disabled',true);
    $('#statuskep').prop('disabled',true);
  @endif

  $('#uuid_jabatan').select2({
    placeholder: "-- PILIH JABATAN --",
    allowClear: true,
    theme: 'bootstrap4'
  });

  $('#agama').select2({
    placeholder: "-- PILIH AGAMA --",
    allowClear: true,
    theme: 'bootstrap4'
  });

  $('#jk').select2({
    placeholder: "-- PILIH JENIS KELAMIN --",
    allowClear: true,
    theme: 'bootstrap4'
  });

  $('#snikah').select2({
    placeholder: "-- PILIH STATUS NIKAH --",
    allowClear: true,
    theme: 'bootstrap4'
  });

  $('#role').select2({
    placeholder: "-- PILIH ROLE --",
    allowClear: true,
    theme: 'bootstrap4'
  });

  $('#statuskep').select2({
    placeholder: "-- PILIH STATUS KEPEGAWAIAN --",
    allowClear: true,
    theme: 'bootstrap4'
  });

  $('#preview').click(function(){
    $('input[type=file]').click();
  });

  $('#foto').on('change',function() {
    renderImg(this);
  })

  let reset = () => {
    $('#pegawai_form')[0].reset();
    $('#uuid_jabatan').val('').trigger('change');
    $('#agama').val('').trigger('change');
    $('#jk').val('').trigger('change');
    $('#snikah').val('').trigger('change');
    $('#statuskep').val('').trigger('change');
    $('#preview').attr('src',"{{ url('/').'/assets/photos/default.png' }}")
    $('#aksi').val('simpan');
  }

  let reset_update = () => {
    $('#aksi').val('update');
  }

  let SaveRecord = (_url, _form) => {
    Swal.fire({
      title: 'Apakah Anda Yakin Menyimpan Data Ini ?',
      showCancelButton: true,
      confirmButtonText: `Simpan`,
      confirmButtonColor: '#d33',
      icon: 'question'
    }).then((result) =>{
      if(result.isConfirmed) {
        $.ajax({
          url: _url,
          method: "POST",
          data: _form,
          contentType: false,
          cache: false,
          processData: false,
          dataType: "json",
          success:function(data) {
            var count
            if(data.response == 'success') {
              Swal.fire({
                type: 'success',
                title: data.header,
                text: data.messages,
                timer: 3000,
                icon: 'success',
                showCancelButton: false,
                showConfirmButton: false
              }).then (function() {
                  window.location.href = "{{ route('pegawai.index') }}";
              });
            } else if(data.response == 'failed') {
              var messages = data.messages.join('<br/>')
              //console.log(messages)
              // Swal.fire(
              //   data.header,
              //   messages,
              //   'error'
              // )
              toastr.error(messages);
            }
          }
        });
      } else if(result.isDenied) {
        Swal.fire('Changes are not saved', '', 'info')
      }
    })
  }

  $('#reset1').click(function() {
    @if(!empty($data['readonly']))
      reset_update();
    @elseif(empty($data['readonly']))
      reset();
    @endif
  });

  $('#back').click(function () {
    @if(Auth::user()->role->id == 1)
        window.location.href = '{{ route("pegawai.index") }}';
    @elseif(Auth::user()->role->id != 1)
        window.location.href = '{{ route("dashboard.index") }}';
    @endif
  });

  $('#pegawai_form').on('submit', function() {
    event.preventDefault();
    var _url_add = '{{ route("pegawai.store") }}';
    var _url_update = '{{ route("pegawai.update") }}';
    let _table = $('#skp')
    let _form_id = $('#skp_form')
    let _form = new FormData($(this)[0]);
    if($('#aksi').val() == 'simpan') {
      SaveRecord(_url_add, _form);
    } else if($('#aksi').val() == 'update') {
      SaveRecord(_url_update, _form);
    }
  });

  function renderImg(input) {
  	if (input.files && input.files[0]) {
  	   var reader = new FileReader();
  			reader.onload = function(e) {
  			  $('#preview').attr('src', e.target.result);
  			}
  			reader.readAsDataURL(input.files[0]);
  		}
  	}





});

</script>
@endpush

@endsection
