@extends('AdminLTE.app')

@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Data Management</h3>
      </div>
          @csrf
          <div class="card-body">
            <div class="form-group">
              <div class="row">
                <div class="col-3">
                </div>
                <div class="col-6">
                  <button type="button" id = "backup" class="btn btn-success"><i class="fa fa-download"></i> Export</button>
                  <button type="button" id = "import" class="btn btn-primary"><i class="fa fa-upload"></i> Import</button>
                  <button type="button" id = "import" class="btn btn-danger"><i class="fa fa-trash"></i> Kosongkan Data</button>
                </div>
                <div class="col-3">
                </div>
              </div>
            </div>
          </div>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="row">
        <div class="col-10">
        </div>
        <div class="col-2">
          <button type="button" id = "tambah" class="btn btn-sm btn-success float-right"><i class="fa fa-plus"></i> Tambah</button>
        </div>
      </div>
    </div>
  </div>

  <br/>

<div class="row">
  <div class="col-md-12">
    <table id="pegawai" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>No</th>
          <th>NIP Pegawai</th>
          <th>Nama Pegawai</th>
          <th>Jabatan Pegawai</th>
          <th>Waktu Input</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <td>No</td>
          <td>NIP Pegawai</td>
          <td>Nama Pegawai</td>
          <td>Jabatan Pegawai</td>
          <td>Waktu Input</td>
          <td>Aksi</td>
        </tr>
      </tfoot>
    </table>
  </div>
</div>

@push('script')
<script>
$(document).ready(function() {

  var _url = '{{ route("pegawai.dataTables") }}'
  var _table = $('#pegawai')
  var _column = ['DT_RowIndex','nip_pegawai','nama_pegawai','jabatan.nama_jabatan','created_at','action']
  // var _form_id = $('#kategori_form')
  CustomDataTables(_table,_url,null,_column)

  $('#tambah').click(function(){
    // setTimeout(function() {
      $.ajax({
        url: '{{ route("pegawai.requestAct","add") }}',
        method: 'POST',
        success: function(data) {
          if(data.response == 'success') {
            window.location.href = '{{ route("pegawai.create") }}';
          } else if(data.response == 'failed'){
            Swal.fire({
              type: 'error',
              title: 'Oops...',
              text: 'Gagal Memanggil Halaman',
              icon: 'error'
            });
          }
        },
        error: function(data) {
          Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Error',
            icon: 'error'
          });
        }
      });
    // }, 5000);
  })

  // _form_id.on('submit', function() {
  //   event.preventDefault();
  //   var _url_add = '{{ route("kategori.store") }}'
  //   let _table = $('#kategori')
  //   let _form_id = $('#kategori_form')
  //   let _form = new FormData($(this)[0]);
  //   SendData(_form, _url_add, _form_id, null, _table)
  // });
  //
  $(document).on('click','.delete',function() {
    var _url_delete = $(this).attr('id')
    // console.log(_url_delete)
    DeleteData(_url_delete,_table)
  });

  $(document).on('click','.update',function() {
    var _url_update = $(this).attr('id')
    $.ajax({
      url: '{{ route("pegawai.requestAct","upd") }}',
      method: 'POST',
      success: function(data) {
        if(data.response == 'success') {
          window.location.href = _url_update;
        } else if(data.response == 'failed'){
          Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Gagal Memanggil Halaman',
            icon: 'error'
          });
        }
      },
      error: function(data) {
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Error',
          icon: 'error'
        });
      }
    });
  });

  $(document).on('click','.view',function() {
    var _url_view = $(this).attr('id')
    $.ajax({
      url: '{{ route("pegawai.requestAct","read") }}',
      method: 'POST',
      success: function(data) {
        if(data.response == 'success') {
          window.location.href = _url_view;
        } else if(data.response == 'failed'){
          Swal.fire({
            type: 'error',
            title: 'Oops...',
            text: 'Gagal Memanggil Halaman',
            icon: 'error'
          });
        }
      },
      error: function(data) {
        Swal.fire({
          type: 'error',
          title: 'Oops...',
          text: 'Error',
          icon: 'error'
        });
      }
    });
  });

  $(document).on('click','.akses',function() {
    var _url_akses = $(this).attr('id')
    // console.log(_url_delete)
    Swal.fire({
      title: 'Apakah Anda Yakin Menambahkan Akses ?',
      // showDenyButton: true,
      showCancelButton: true,
      confirmButtonText: `Simpan`,
      confirmButtonColor: '#d33',
      // denyButtonText: `Kembali`,
      icon: 'question'
    }).then((result) =>{
      if(result.isConfirmed) {
        $.ajax({
          url: _url_akses,
          method: "POST",
          dataType: "json",
          success:function(data) {
            var count
            if(data.response == 'success') {
              Swal.fire(
                data.header,
                data.messages,
                'success'
              )
              $('#pegawai').DataTable().ajax.reload();
            } else if(data.response == 'failed') {
              var messages = data.messages.join('<br/>')
              //console.log(messages)
              Swal.fire(
                data.header,
                messages,
                'error'
              )
            }
          }
        });
      } else if(result.isDenied) {
        Swal.fire('Changes are not saved', '', 'info')
      }
    })
  });

});

</script>
@endpush

@endsection
