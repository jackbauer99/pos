@extends('AdminLTE.app')

@section('content')

<!-- Small boxes (Stat box) -->
<div class="row">
  <div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-info">
      <div class="inner">
        <h3>{{ $data['pegawai'] }}</h3>

        <p>Jumlah Pegawai</p>
      </div>
      <div class="icon">
        <i class="fa fa-user"></i>
      </div>
      <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <!-- ./col -->
  <div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-success">
      <div class="inner">
        <h3>{{ $data['user'] }}</h3>

        <p>Jumlah User Aktif</p>
      </div>
      <div class="icon">
        <i class="fa fa-user-check"></i>
      </div>
      <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <!-- ./col -->
  <div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-warning">
      <div class="inner">
        <h3>{{ $data['pegawai']-$data['user'] }}</h3>

        <p>Jumlah User Tidak Aktif</p>
      </div>
      <div class="icon">
        <i class="ion ion-person-add"></i>
      </div>
      <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <!-- ./col -->
  <div class="col-lg-3 col-6">
    <!-- small box -->
    <div class="small-box bg-danger">
      <div class="inner">
        <h3>65</h3>

        <p>Unique Visitors</p>
      </div>
      <div class="icon">
        <i class="ion ion-pie-graph"></i>
      </div>
      <a href="#" class="small-box-footer">More info <i class="fas fa-arrow-circle-right"></i></a>
    </div>
  </div>
  <!-- ./col -->
</div>
<!-- /.row -->

@if(Auth::user()->role->id == 1)
<div class="row">
  <div class="col-md-12">
    <table id="log" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>No</th>
          <th>Nama User</th>
          <th>Aktivitas</th>
          <th>IP Address</th>
          <th>Device</th>
          <th>Browser</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <th>No</th>
          <th>Nama User</th>
          <th>Aktivitas</th>
          <th>IP Address</th>
          <th>Device</th>
          <th>Browser</th>
          <th>Aksi</th>
        </tr>
      </tfoot>
    </table>
  </div>
</div>

@push('script')
<script>
$(document).ready(function() {

  var _url = '{{ route("loglogin.dataTables") }}'
  var _table = $('#log')
  var _column = ['DT_RowIndex','pegawai.nama_pegawai','activity','user_ipaddress','user_device','user_browser','action']
  // var _form_id = $('#kategori_form')
  CustomDataTables(_table,_url,null,_column)

});

</script>
@endpush
@endif


@endsection
