@extends('AdminLTE.app')

@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Tambah Kategori</h3>
      </div>
        <form id="kategori_form" method="post" enctype="multipart/form-data">
          @csrf
          <div class="card-body">
            <div class="form-group">
              <div class="row">
                <div class="col-8">
                  <input type="text" class="form-control" id="kategori_nama" name="kategori_nama" placeholder="Nama Kategori">
                </div>
                <div class="col-3">
                  <button type="reset" id = "reset1" class="btn btn-warning float-right"><i class="fa fa-redo"></i> Reset</button>
                  <button type="submit" id = "insert" class="btn btn-primary float-right"><i class="fa fa-save"></i> Simpan</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

<div class="row">
  <div class="col-md-12">
    <table id="kategori" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>No</th>
          <th>Nama Kategori</th>
          <th>Waktu Input</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <td>No</td>
          <td>Nama Kategori</td>
          <td>Waktu Input</td>
          <td>Aksi</td>
        </tr>
      </tfoot>
    </table>
  </div>
</div>


@push('script')
<script>
$(document).ready(function() {

  var _url = '{{ route("kategori.dataTables") }}'
  var _table = $('#kategori')
  var _column = ['DT_RowIndex','nama_kategori','created_at','action']
  var _form_id = $('#kategori_form')
  CustomDataTables(_table,_url,null,_column)

  _form_id.on('submit', function() {
    event.preventDefault();
    var _url_add = '{{ route("kategori.store") }}'
    let _table = $('#kategori')
    let _form_id = $('#kategori_form')
    let _form = new FormData($(this)[0]);
    SendData(_form, _url_add, _form_id, null, _table)
  });

  $(document).on('click','.delete',function() {
    var _url_delete = $(this).attr('id')
    // console.log(_url_delete)
    DeleteData(_url_delete,_table)
  });
});

</script>
@endpush

@endsection
