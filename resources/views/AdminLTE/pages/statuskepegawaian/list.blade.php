@extends('AdminLTE.app')

@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Form Status Kepegawaian</h3>
      </div>
        <form id="skp_form" method="post" enctype="multipart/form-data">
          @csrf
          <div class="card-body">
            <div class="form-group">
              <div class="row">
                <div class="col-3">
                  <input type="text" class="form-control" id="nama_status_kepegawaian" name="nama_status_kepegawaian" placeholder="Nama Status Kepegawaian" maxlength="15">
                </div>
                <div class="col-3">
                  <input type="text" class="form-control" id="lama_kerja_status_kepegawaian" name="lama_kerja_status_kepegawaian" placeholder="Lama Kerja">
                </div>
                <div class="col-3">
                  <input type="text" class="form-control" id="gaji_pokok_status_kepegawaian" name="gaji_pokok_status_kepegawaian" placeholder="Gaji Pokok Kerja">
                </div>
                <div class="col-3">
                  <input type = "hidden" id = "aksi" value="simpan">
                  <input type = "hidden" id = "uuid" name="uuid">
                  <button type="button" id = "reset1" class="btn btn-warning float-right"><i class="fa fa-redo"></i> Reset</button>
                  <button type="submit" id = "save" class="btn btn-primary float-right"><i class="fa fa-save"></i> Simpan</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Data Management</h3>
        </div>
          <form method="post" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
              <div class="form-group">
                <div class="row">
                  <div class="col-3">
                  </div>
                  <div class="col-6">
                    <button type="button" id = "backup" class="btn btn-success"><i class="fa fa-download"></i> Export</button>
                    <button type="button" id = "import" class="btn btn-primary"><i class="fa fa-upload"></i> Import</button>
                    <button type="button" id = "import" class="btn btn-danger"><i class="fa fa-trash"></i> Kosongkan Data</button>
                  </div>
                  <div class="col-3">
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

<div class="row">
  <div class="col-md-12">
    <table id="skp" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>No</th>
          <th>Nama Status Kepegawaian</th>
          <th>Lama Kerja</th>
          <th>Gaji Pokok</th>
          <th>Waktu Input</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <td>No</td>
          <td>Nama Status Kepegawaian</td>
          <td>Lama Kerja</td>
          <td>Gaji Pokok</td>
          <td>Waktu Input</td>
          <td>Aksi</td>
        </tr>
      </tfoot>
    </table>
  </div>
</div>


@push('script')
<script>
$(document).ready(function() {

  var _url = '{{ route("skp.dataTables") }}'
  var _table = $('#skp')
  var _column = ['DT_RowIndex','nama_status_kepegawaian','lama_kerja_status_kepegawaian','gaji_pokok_status_kepegawaian','created_at','action']
  var _form_id = $('#skp_form')
  CustomDataTables(_table,_url,null,_column)

  _form_id.on('submit', function() {
    event.preventDefault();
    var _url_add = '{{ route("skp.store") }}';
    var _url_upd = '{{ route("skp.update") }}';
    let _table = $('#skp')
    let _form_id = $('#skp_form')
    let _form = new FormData($(this)[0]);
    if($('#aksi').val() == 'simpan') {
        SendData(_form, _url_add, _form_id, null, _table)
    } else if($('#aksi').val() == 'update') {
      SendData(_form, _url_upd, _form_id, null, _table)
      $('#aksi').val('simpan');
    }
  });

  $('#reset1').click(function() {
    $('#skp_form')[0].reset();
    $('#aksi').val('simpan');
  });

  $(document).on('click','.delete',function() {
    var _url_delete = $(this).attr('id')
    // console.log(_url_delete)
    DeleteData(_url_delete,_table)
  });
  //
  $(document).on('click','.update',function() {
    var _url_update = $(this).attr('id')
    console.log(_url_update);
    // console.log(_url_delete)
    $(window).scrollTop(0);
    $('#aksi').val('update');
    $.ajax({
      url: _url_update,
      dataType: "json",
      success: function(data) {
        $('#uuid').val(data.data.uuid);
        $('#nama_status_kepegawaian').val(data.data.nama_status_kepegawaian);
        $('#lama_kerja_status_kepegawaian').val(data.data.lama_kerja_status_kepegawaian);
        $('#gaji_pokok_status_kepegawaian').val(data.data.gaji_pokok_status_kepegawaian);
      }
    });
  });

});

</script>
@endpush

@endsection
