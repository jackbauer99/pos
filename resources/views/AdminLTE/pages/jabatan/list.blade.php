@extends('AdminLTE.app')

@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Form Jabatan</h3>
      </div>
        <form id="jabatan_form" method="post" enctype="multipart/form-data">
          @csrf
          <div class="card-body">
            <div class="form-group">
              <div class="row">
                <div class="col-4">
                  <input type="text" class="form-control" id="kode_jabatan" name="kode_jabatan" placeholder="Kode Jabatan" maxlength="15">
                </div>
                <div class="col-5">
                  <input type="text" class="form-control" id="nama_jabatan" name="nama_jabatan" placeholder="Nama Jabatan">
                </div>
                <div class="col-3">
                  <input type = "hidden" id = "aksi" value="simpan">
                  <input type = "hidden" id = "uuid" name="uuid">
                  <button type="button" id = "reset1" class="btn btn-warning float-right"><i class="fa fa-redo"></i> Reset</button>
                  <button type="submit" id = "save" class="btn btn-primary float-right"><i class="fa fa-save"></i> Simpan</button>
                </div>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>

  <div class="row">
    <div class="col-md-12">
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Data Management</h3>
        </div>
          <form id="jabatan_form" method="post" enctype="multipart/form-data">
            @csrf
            <div class="card-body">
              <div class="form-group">
                <div class="row">
                  <div class="col-3">
                  </div>
                  <div class="col-6">
                    <button type="button" id = "backup" class="btn btn-success"><i class="fa fa-download"></i> Export</button>
                    <button type="button" id = "import" class="btn btn-primary"><i class="fa fa-upload"></i> Import</button>
                    <button type="button" id = "import" class="btn btn-danger"><i class="fa fa-trash"></i> Kosongkan Data</button>
                  </div>
                  <div class="col-3">
                  </div>
                </div>
              </div>
            </div>
          </form>
        </div>
      </div>
    </div>

<div class="row">
  <div class="col-md-12">
    <table id="jabatan" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>No</th>
          <th>Kode Jabatan</th>
          <th>Nama Jabatan</th>
          <th>Waktu Input</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <td>No</td>
          <td>Kode Jabatan</td>
          <td>Nama Jabatan</td>
          <td>Waktu Input</td>
          <td>Aksi</td>
        </tr>
      </tfoot>
    </table>
  </div>
</div>


@push('script')
<script>
$(document).ready(function() {

  var _url = '{{ route("jabatan.dataTables") }}'
  var _table = $('#jabatan')
  var _column = ['DT_RowIndex','kode_jabatan','nama_jabatan','created_at','action']
  var _form_id = $('#jabatan_form')
  CustomDataTables(_table,_url,null,_column)

  _form_id.on('submit', function() {
    event.preventDefault();
    var _url_add = '{{ route("jabatan.store") }}';
    var _url_upd = '{{ route("jabatan.update") }}';
    let _table = $('#jabatan')
    let _form_id = $('#jabatan_form')
    let _form = new FormData($(this)[0]);
    if($('#aksi').val() == 'simpan') {
        SendData(_form, _url_add, _form_id, null, _table)
    } else if($('#aksi').val() == 'update') {
      SendData(_form, _url_upd, _form_id, null, _table)
      $('#aksi').val('simpan');
    }
  });

  $('#reset1').click(function() {
    $('#jabatan_form')[0].reset();
    $('#aksi').val('simpan');
  });

  $(document).on('click','.delete',function() {
    var _url_delete = $(this).attr('id')
    // console.log(_url_delete)
    DeleteData(_url_delete,_table)
  });

  $(document).on('click','.update',function() {
    var _url_update = $(this).attr('id')
    console.log(_url_update);
    // console.log(_url_delete)
    $(window).scrollTop(0);
    $('#aksi').val('update');
    $.ajax({
      url: _url_update,
      dataType: "json",
      success: function(data) {
        $('#uuid').val(data.data.uuid);
        $('#kode_jabatan').val(data.data.kode_jabatan);
        $('#nama_jabatan').val(data.data.nama_jabatan);
      }
    });
  });

});

</script>
@endpush

@endsection
