@extends('AdminLTE.app')

@section('content')

<div class="row">
  <div class="col-md-12">
    <div class="card card-primary">
      <div class="card-header">
        <h3 class="card-title">Tambah Barang</h3>
      </div>
        <form id="barang_form" method="post" enctype="multipart/form-data">
          @csrf
          <div class="card-body">
            <div class="form-group">
              <div class="row">
                <div class="col-6">
                  <input type="text" class="form-control" id="kode_barang" name="kode_barang" placeholder="Kode Barang">
                </div>
                <div class="col-6">
                  <input type="text" class="form-control" id="nama_barang" name="nama_barang" placeholder="Nama Barang">
                </div>
              </div>
            </br>
              <div class="row">
                <div class="col-6">
                  <input type="text" class="form-control" id="manufaktur_barang" name="manufaktur_barang" placeholder="Manufaktur Barang">
                </div>
                <div class="col-6">
                  <select class="form-control select2" style="width: 100%;" id= "uuid_kategori" name="uuid_kategori">
                    <option></option>
                    @foreach($data['kategori'] as $kategori)
                      <option value="{{ $kategori->uuid }}">{{ $kategori->nama_kategori }}</option>
                    @endforeach
                  </select>
                </div>
              </div>
            <br/>
            <div class="row">
              <div class="col-6">
                <input type="number" class="form-control" id="jumlah_stok_barang" name="jumlah_stok_barang" placeholder="Jumlah Stok Barang">
              </div>
              <div class="col-6">
                <input type="number" class="form-control" id="harga_per_satuan_barang" name="harga_per_satuan_barang" placeholder="Harga Per Satuan Barang">
              </div>
            </div>
            <br/>
            <div class="row">
              <div class="col-9">
              </div>
              <div class="col-3">
                <input type = "hidden" id = "aksi" value="simpan">
                <input type = "hidden" id = "uuid" name="uuid">
                <button type="submit" id = "save" class="btn btn-primary"><i class="fa fa-save"></i> Simpan</button>
                <button type="button" id = "reset1" class="btn btn-warning"><i class="fa fa-redo"></i> Reset</button>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>

  <div class="row">
    <div class="col-md-12">
      <div class="card card-primary">
        <div class="card-header">
          <h3 class="card-title">Data Management</h3>
        </div>
            @csrf
            <div class="card-body">
              <div class="form-group">
                <div class="row">
                  <div class="col-3">
                  </div>
                  <div class="col-6">
                    <button type="button" id = "backup" class="btn btn-success"><i class="fa fa-download"></i> Export</button>
                    <button type="button" id = "import" class="btn btn-primary"><i class="fa fa-upload"></i> Import</button>
                    <button type="button" id = "import" class="btn btn-danger"><i class="fa fa-trash"></i> Kosongkan Data</button>
                  </div>
                  <div class="col-3">
                  </div>
                </div>
              </div>
            </div>
        </div>
      </div>
    </div>

<div class="row">
  <div class="col-md-12">
    <table id="barang" class="table table-bordered table-striped">
      <thead>
        <tr>
          <th>No</th>
          <th>Kode Barang</th>
          <th>Nama Barang</th>
          <th>Kategori Barang</th>
          <th>Manufaktur Barang</th>
          <th>Stok Barang</th>
          <th>Harga Barang</th>
          <th>Aksi</th>
        </tr>
      </thead>
      <tfoot>
        <tr>
          <td>No</td>
          <td>Kode Barang</td>
          <td>Nama Barang</td>
          <td>Kategori Barang</td>
          <td>Manufaktur Barang</td>
          <td>Stok Barang</td>
          <td>Harga Barang</td>
          <td>Aksi</td>
        </tr>
      </tfoot>
    </table>
  </div>
</div>


@push('script')
<script>
$(document).ready(function() {

  $('.select2').select2({
    placeholder: "-- PILIH KATEGORI --",
    allowClear: true,
    theme: 'bootstrap4'
  });

  var _url = '{{ route("barang.dataTables") }}'
  var _table = $('#barang')
  var _column = ['DT_RowIndex','kode_barang','nama_barang','kategori_barang.nama_kategori','manufaktur_barang','jumlah_stok_barang','harga_per_satuan_barang','action']
  var _form_id = $('#barang_form')
  CustomDataTables(_table,_url,null,_column)

  _form_id.on('submit', function() {
    event.preventDefault();
    var _url_add = '{{ route("barang.store") }}'
    var _url_upd = '{{ route("barang.update") }}'
    let _table = $('#barang')
    let _form_id = $('#barang_form')
    let _form = new FormData($(this)[0]);
    let _select2 = $('.select2');
    if($('#aksi').val() == 'simpan') {
        SendData(_form, _url_add, _form_id, _select2, _table)
    } else if($('#aksi').val() == 'update') {
      SendData(_form, _url_upd, _form_id, _select2, _table)
      $('#aksi').val('simpan');
    }
  });

  $('#reset1').click(function() {
    $('.select2').val('').trigger('change');
    $('#barang_form')[0].reset();
    $('#aksi').val('simpan');
  });

  $(document).on('click','.delete',function() {
    var _url_delete = $(this).attr('id')
    // console.log(_url_delete)
    DeleteData(_url_delete,_table)
  });

  $(document).on('click','.update',function() {
    var _url_update = $(this).attr('id')
    console.log(_url_update);
    // console.log(_url_delete)
    $(window).scrollTop(0);
    $('#aksi').val('update');
    $.ajax({
      url: _url_update,
      dataType: "json",
      success: function(data) {
        $('#uuid').val(data.data.uuid);
        $('#kode_barang').val(data.data.kode_barang);
        $('#nama_barang').val(data.data.nama_barang);
        $('#manufaktur_barang').val(data.data.manufaktur_barang);
        $('.select2').val(data.data.uuid_kategori).trigger('change');
        $('#jumlah_stok_barang').val(data.data.jumlah_stok_barang);
        $('#harga_per_satuan_barang').val(data.data.harga_per_satuan_barang);
      }
    });
  });
});

</script>
@endpush

@endsection
