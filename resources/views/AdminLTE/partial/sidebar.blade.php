

<!-- Brand Logo -->
<a href="../../index3.html" class="brand-link">
  <span class="brand-text font-weight-light">AdminLTE 3</span>
</a>

<!-- Sidebar -->
<div class="sidebar">
  <!-- Sidebar user (optional) -->
  <div class="user-panel mt-3 pb-3 mb-3 d-flex">
    <!-- <div class="image"> -->
      <img src="{{ url('/').Auth::user()->pegawai->foto_pegawai }}" width="200" height="200" class="img-circle elevation-2" alt="">
    <!-- </div> -->
    <div class="info">
      <a href="{{ route('pegawai.show',Auth::user()->uuid_pegawai) }}" class="d-block">{{ Auth::user()->pegawai->nama_pegawai }}</a>
    </div>
  </div>

  <!-- Sidebar Menu -->
  <nav class="mt-2">
    <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
      <!-- Add icons to the links using the .nav-icon class
           with font-awesome or any other icon font library -->
      @foreach(App\Models\Menu::SideBarMenu() as $key => $value)
        @foreach($value as $key2 => $value2)
          @if(empty($value2['access']))
          <li class="nav-item has-treeview">
            @if(!empty($value2['url']))
              <a href="{{ route($value2['url']) }}" class="nav-link">
            @else
              <a href="#" class="nav-link">
            @endif
              <i class="{{ $value2['icon'] }}"></i>
              <p>
                  {{ $key2 }}
                  @if(!empty($value2['childmenu']))
                    <i class="right fas fa-angle-left"></i>
                  @endif
              </p>
            </a>
          @if(!empty($value2['childmenu']))
              <ul class="nav nav-treeview">
            @foreach($value2['childmenu'] as $key3 => $value3)
                <li class="nav-item">
                  <a href="{{ route($value2['childmenu'][$key3]) }}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                      <p>{{ $key3 }}</p>
                  </a>
                </li>
            @endforeach
              </ul>
          @endif
      </li>  
          @elseif(Auth::user()->role->id == $value2['access'])
          <li class="nav-item has-treeview">
            @if(!empty($value2['url']))
              <a href="{{ route($value2['url']) }}" class="nav-link">
            @else
              <a href="#" class="nav-link">
            @endif
              <i class="{{ $value2['icon'] }}"></i>
              <p>
                  {{ $key2 }}
                  @if(!empty($value2['childmenu']))
                    <i class="right fas fa-angle-left"></i>
                  @endif
              </p>
            </a>
          @if(!empty($value2['childmenu']))
              <ul class="nav nav-treeview">
            @foreach($value2['childmenu'] as $key3 => $value3)
                <li class="nav-item">
                  <a href="{{ route($value2['childmenu'][$key3]) }}" class="nav-link">
                    <i class="far fa-circle nav-icon"></i>
                      <p>{{ $key3 }}</p>
                  </a>
                </li>
            @endforeach
              </ul>
          @endif
      </li>
      @endif
      @endforeach
    @endforeach
    </ul>
  </nav>
  <!-- /.sidebar-menu -->
</div>
<!-- /.sidebar -->

@push('script')
<script type="text/javascript">
	$(document).ready(function() {
    let menu = $('.nav.nav-pills.nav-sidebar.flex-column').find('a');

    // var str = window.location.href;
    // var expl = str.split("/");
    // console.log(expl);

		for (var i = 0; i < menu.length; i++) {
			href = menu.eq(i).attr('href');

			// if (window.location.href == href) {
      //
			// }
      var str = window.location.href;
      var expl = str.split("/");
      var sts = expl[0] + "/" + expl[1] + "/" + expl[2] + "/" + expl[3];

      if(href == sts) {
        menu.eq(i).addClass('active');

        menu.eq(i).parents('li')
          .parents('li')
          .addClass('menu-open')
          .children('a')
          .addClass('active');
      }
		}
	});
</script>
@endpush
