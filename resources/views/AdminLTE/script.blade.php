<!-- jQuery -->
<script src="{{ url('assets/AdminLTE') }}/plugins/jquery/jquery.min.js"></script>
<!-- jQuery UI 1.11.4 -->
<script src="{{ url('assets/AdminLTE') }}/plugins/jquery-ui/jquery-ui.min.js"></script>
<!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
<script>
  $.widget.bridge('uibutton', $.ui.button)
</script>
<!-- Bootstrap 4 -->
<script src="{{ url('assets/AdminLTE') }}/plugins/bootstrap/js/bootstrap.bundle.min.js"></script>
<!-- ChartJS -->
<script src="{{ url('assets/AdminLTE') }}/plugins/chart.js/Chart.min.js"></script>
<!-- Sparkline -->
<script src="{{ url('assets/AdminLTE') }}/plugins/sparklines/sparkline.js"></script>
<!-- JQVMap -->
<script src="{{ url('assets/AdminLTE') }}/plugins/jqvmap/jquery.vmap.min.js"></script>
<script src="{{ url('assets/AdminLTE') }}/plugins/jqvmap/maps/jquery.vmap.usa.js"></script>
<!-- Toastr -->
<script src="{{ url('assets/AdminLTE') }}/plugins/toastr/toastr.min.js"></script>
<!-- jQuery Knob Chart -->
<script src="{{ url('assets/AdminLTE') }}/plugins/jquery-knob/jquery.knob.min.js"></script>
<!-- daterangepicker -->
<script src="{{ url('assets/AdminLTE') }}/plugins/moment/moment.min.js"></script>
<script src="{{ url('assets/AdminLTE') }}/plugins/daterangepicker/daterangepicker.js"></script>
<!-- Tempusdominus Bootstrap 4 -->
<script src="{{ url('assets/AdminLTE') }}/plugins/tempusdominus-bootstrap-4/js/tempusdominus-bootstrap-4.min.js"></script>
<!-- Summernote -->
<script src="{{ url('assets/AdminLTE') }}/plugins/summernote/summernote-bs4.min.js"></script>
<!-- overlayScrollbars -->
<script src="{{ url('assets/AdminLTE') }}/plugins/overlayScrollbars/js/jquery.overlayScrollbars.min.js"></script>
<!-- AdminLTE App -->
<script src="{{ url('assets/AdminLTE') }}/js/adminlte.js"></script>
<!-- AdminLTE dashboard demo (This is only for demo purposes) -->
<script src="{{ url('assets/AdminLTE') }}/js/pages/dashboard.js"></script>
<!-- AdminLTE for demo purposes -->
<script src="{{ url('assets/AdminLTE') }}/js/demo.js"></script>
<!-- DataTables -->
<script src="{{ url('assets/AdminLTE') }}/plugins/datatables/jquery.dataTables.js"></script>
<script src="{{ url('assets/AdminLTE') }}/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
<!-- jquery-validation -->
<script src="{{ url('assets/AdminLTE') }}/plugins/jquery-validation/jquery.validate.min.js"></script>
<script src="{{ url('assets/AdminLTE') }}/plugins/jquery-validation/additional-methods.min.js"></script>
<!-- bootstrap color picker -->
<script src="{{ url('assets/AdminLTE') }}/plugins/bootstrap-colorpicker/js/bootstrap-colorpicker.min.js"></script>
<!-- Select2 -->
<script src="{{ url('assets/AdminLTE') }}/plugins/select2/js/select2.full.min.js"></script>
<!-- SweetAlert2 -->
<script src="{{ url('assets/AdminLTE') }}/plugins/sweetalert2-v2/dist/sweetalert2.min.js"></script>
<!-- Date Picker -->
<script src="{{ url('assets/AdminLTE') }}/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js"></script>
<!-- BootBox -->
<script src="{{ url('assets/AdminLTE') }}/plugins/bootbox/bootbox.min.js"></script>
<script src="{{ url('assets/AdminLTE') }}/plugins/bootbox/bootbox.locales.min.js"></script>
<!-- bs-custom-file-input -->
<script src="{{ url('assets/AdminLTE') }}/plugins/bs-custom-file-input/bs-custom-file-input.min.js"></script>
<script src="{{ url('assets/AdminLTE') }}/plugins/blockUI/jquery.blockUI.js"></script>
<script src="{{ url('assets/customjs') }}/datatablecustom.js"></script>
<!-- Leaflet JS -->
<script src="{{ url('assets/customjs') }}/leaflet/leaflet.js"></script>
<script src="{{ url('assets/customjs') }}/leaflet-routing/leaflet-routing-machine.js"></script>
<script>
$.ajaxSetup({
    headers: {
        'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
    }
});
</script>
@stack('script')
