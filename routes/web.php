<?php

use Illuminate\Support\Facades\Route;
use Auth;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// Route::get('/', function () {
//     return view('welcome');
// });

Route::middleware(['notLogin'])->group(function() {
  Route::get('dashboard','MainController@index')->name('dashboard.index');
  Route::post('loglogin/dataTables','MainController@datatables')->name('loglogin.dataTables');
  Route::post('auth/requestLoginUser/{auth}','AuthController@UserRequestLogin')->name('auth.requestLoginUser');
  Route::resource('kategori','KategoriController');
  Route::resource('barang','BarangController');

  Route::middleware(['slugpermission'])->group(function() {
    Route::resource('pegawai','PegawaiController');
    Route::resource('jabatan','JabatanController');
    Route::get('skp','StatusKepegawaianController@index')->name('skp.index');
    Route::post('jabatan/update','JabatanController@update')->name('jabatan.update');
    Route::post('pegawai/update','PegawaiController@update')->name('pegawai.update');
    Route::post('pegawai/dataTables','PegawaiController@datatables')->name('pegawai.dataTables');
    Route::post('jabatan/dataTables','JabatanController@datatables')->name('jabatan.dataTables');
    Route::post('skp/dataTables','StatusKepegawaianController@datatables')->name('skp.dataTables');
    Route::post('pegawai/requestAct/{pegawai}','PegawaiController@requestAct')->name('pegawai.requestAct');
    Route::post('skp','StatusKepegawaianController@store')->name('skp.store');
    Route::get('skp/{skp}/edit','StatusKepegawaianController@edit')->name('skp.edit');
    Route::post('skp/update','StatusKepegawaianController@update')->name('skp.update');
    Route::delete('skp/{skp}','StatusKepegawaianController@destroy')->name('skp.destroy');
  });

  Route::get('pegawai/{pegawai}','PegawaiController@show')->name('pegawai.show');
  Route::post('barang/update','BarangController@update')->name('barang.update');
  Route::post('kategori/dataTables','KategoriController@datatables')->name('kategori.dataTables');
  Route::post('barang/dataTables','BarangController@datatables')->name('barang.dataTables');
  Route::get('leaflet/maps','LeafletController@index')->name('leaflet.index');
});
