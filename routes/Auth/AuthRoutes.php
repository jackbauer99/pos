<?php

use Illuminate\Support\Facades\Route;

Route::get('auth/logout/{auth}','AuthController@logout')->name('auth.logout');

Route::middleware(['isLogin'])->group(function() {
  Route::get('auth/login','AuthController@index')->name('auth.login');
  Route::post('auth/login','AuthController@getLogin')->name('auth.login');
});



 ?>
